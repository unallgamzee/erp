# Django
from django.core.paginator import Paginator

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Company
from api.expensive.serializers import ExpensiveSummarySerializer
from api.expensive.models import Expensive

# General
from api.field_names import ExpensiveFields

from rest_framework.permissions import IsAuthenticated


class ExpensiveListView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):

        Expensives = Expensive.objects.all()

        serializer = ExpensiveSummarySerializer(Expensives, many=True, read_only=True)
        response_data = {
            "expensives": serializer.data
        }

        return Response(response_data, status=status.HTTP_200_OK)