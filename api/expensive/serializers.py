from rest_framework import serializers

# Expensive
from api.expensive.models import Expensive

# General
from api.field_names import ExpensiveFields

# Serializers
from api.user.serializers import UserSummarySerializer


class ExpensiveCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Expensive
        fields = (
            ExpensiveFields.description,
            ExpensiveFields.price,
            ExpensiveFields.currency,
            ExpensiveFields.expense_date,
            ExpensiveFields.insert_user
        )


class ExpensiveSummarySerializer(serializers.ModelSerializer):

    insert_user = UserSummarySerializer(many=False, read_only=True)

    class Meta:
        model = Expensive
        fields = (
            ExpensiveFields.expensive_id,
            ExpensiveFields.description,
            ExpensiveFields.price,
            ExpensiveFields.currency,
            ExpensiveFields.expense_date,
            ExpensiveFields.insert_user,
            ExpensiveFields.insert_date
        )


class ExpensiveUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Expensive
        fields = (
            ExpensiveFields.description,
            ExpensiveFields.price,
            ExpensiveFields.currency,
            ExpensiveFields.expense_date
        )
