# Django
from django.conf.urls import url

# Expensive
from api.expensive.new import ExpensiveCreateView
from api.expensive.detail import ExpensiveDetailView
from api.expensive.list import ExpensiveListView

urlpatterns = [
    url(r'^expensives/new/$', ExpensiveCreateView.as_view(), name="expensive-new"),
    url(r'^expensives/$', ExpensiveListView.as_view(), name="expensive-list"),
    url(r'^expensives/(?P<expensive_id>[0-9]+)/$', ExpensiveDetailView.as_view(), name="expensive-detail")
]
