# Django
from django.db import models

from api.user.models import User

from django.utils.timezone import now
from api.field_names import CommonChoices


class Expensive(models.Model):
    description = models.TextField(max_length=256)

    price = models.FloatField(blank=True, null=False)
    currency = models.IntegerField(null=False, default=2, choices=CommonChoices.CURRENCY_CHOICES)

    expense_date = models.DateTimeField(default=now, editable=False)

    insert_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="insert_expensive")
    insert_date = models.DateTimeField(default=now, editable=False)
    
    @staticmethod
    def get_expensive(**kwargs):
        """
        @brief    If the expensive with the given arguments exists, returns the expensive. Otherwise returns None
        """

        company = None
        try:
            expensive = Expensive.objects.get(**kwargs)
        except Expensive.DoesNotExist:
            pass

        return expensive
