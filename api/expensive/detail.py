# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Expensive
from api.expensive.models import Expensive

# Serializers
from api.expensive.serializers import ExpensiveSummarySerializer, ExpensiveUpdateSerializer

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC
from api.field_names import ExpensiveFields
import datetime

from rest_framework.permissions import IsAuthenticated


class ExpensiveDetailView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, expensive_id, format=None):
        expensive = Expensive.get_expensive(id=expensive_id)
        if expensive is not None:
            serializer = ExpensiveSummarySerializer(expensive, many=False, read_only=True)
            response_data = {
                "expensive": serializer.data
            }

            response = Response(response_data, status=status.HTTP_200_OK)

        else:
            errors = {
                "expensive-id": [
                    _("Expensive ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A expensive with the given ID doesn't exist."), SC.expensive_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response


    def delete(self, request, expensive_id, format=None):
        expensive = Expensive.get_expensive(id=expensive_id)
        if expensive is not None:
            serializer = ExpensiveSummarySerializer(expensive, many=False, read_only=True)
            response_data = {
                "expensive": serializer.data
            }

            expensive.delete()
            response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "expensive-id": [
                    _("Expensive ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A expensive with the given ID doesn't exist."), SC.expensive_not_exist, errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response


    def put(self, request, expensive_id, format=None):
        saved_expensive = None
        payload = request.data

        expensive = Expensive.get_expensive(id=expensive_id)
        if expensive is not None:
            expensive_data = self.extract_expensive_data(payload, expensive)
            if len(expensive_data) == 0:
                response = Response(status=status.HTTP_304_NOT_MODIFIED)
            else:
                errors = {}
                expensive_serializer = ExpensiveUpdateSerializer(expensive, data=expensive_data, partial=True, many=False)
                if expensive_serializer.is_valid():
                    saved_expensive = expensive_serializer.save()
                    saved_expensive.save()
                else:
                    errors.update(expensive_serializer.errors)

                if len(errors) > 0:
                    generated_status = SG.get_status(_("Error occurred while updating the expensive."), SC.parameter_error,
                                                     errors)
                    response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)
                else:
                    detail_serializer = ExpensiveSummarySerializer(saved_expensive, read_only=True, many=False)
                    response_data = {
                        "expensive": detail_serializer.data
                    }
                    response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "expensive-id": [
                    _("Expensive ID doesn't exist.")
                ]
            }
            generated_status = SG.get_status(_("A expensive with the given ID doesn't exist."), SC.expensive_not_exist, errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response


    def extract_expensive_data(self, payload, expensive):
        expensive_data = {}
        if ExpensiveFields.description in payload:
            temp_value = payload.pop(ExpensiveFields.nadescriptionme)
            if expensive.description != temp_value:
                expensive_data[ExpensiveFields.description] = temp_value

        if ExpensiveFields.price in payload:
            temp_value = payload.pop(ExpensiveFields.price)
            if expensive.price != temp_value:
                expensive_data[ExpensiveFields.price] = temp_value

        if ExpensiveFields.currency in payload:
            temp_value = payload.pop(ExpensiveFields.currency)
            if expensive.currency != temp_value:
                expensive_data[ExpensiveFields.fcurrencyax] = temp_value

        if ExpensiveFields.expense_date in payload:
            temp_value = payload.pop(ExpensiveFields.expense_date)
            if expensive.expense_date != temp_value:
                expensive_data[ExpensiveFields.expense_date] = temp_value
        return expensive_data
