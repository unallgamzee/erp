from rest_framework import serializers

# Contact
from api.contact.models import Contact

# General
from api.field_names import ContactFields

# Serializers
from api.company.serializers import CompanySummarySerializer
from api.user.serializers import UserSummarySerializer
from api.country.serializers import CountrySerializer, CitySerializer


class ContactCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = (
            ContactFields.title,
            ContactFields.first_name,
            ContactFields.last_name,

            ContactFields.email,
            ContactFields.phone,

            ContactFields.company,
            ContactFields.is_owner,

            ContactFields.country,
            ContactFields.city,

            ContactFields.description,
            ContactFields.insert_user,
            ContactFields.contact_user
        )


class ContactSummarySerializer(serializers.ModelSerializer):

    company = CompanySummarySerializer(many=False, read_only=True)
    country = CountrySerializer(many=False, read_only=True)
    city = CitySerializer(many=False, read_only=True)
    insert_user = UserSummarySerializer(many=False, read_only=True)
    contact_user = UserSummarySerializer(many=False, read_only=True)

    class Meta:
        model = Contact
        fields = (
            ContactFields.contact_id,
            ContactFields.title,

            ContactFields.first_name,
            ContactFields.last_name,

            ContactFields.email,
            ContactFields.phone,

            ContactFields.company,
            ContactFields.is_owner,

            ContactFields.country,
            ContactFields.city,

            ContactFields.description,
            ContactFields.is_active,

            ContactFields.insert_user,
            ContactFields.insert_date,
            ContactFields.contact_user
        )


class ContactUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Contact
        fields = (
            ContactFields.title,
            ContactFields.first_name,
            ContactFields.last_name,

            ContactFields.email,
            ContactFields.phone,

            ContactFields.company,
            ContactFields.is_owner,

            ContactFields.country,
            ContactFields.city,

            ContactFields.description,
            ContactFields.contact_user
        )
