# Django
from django.core.paginator import Paginator

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Contact
from api.contact.serializers import ContactSummarySerializer
from api.contact.models import Contact

# General
from api.field_names import ContactFields

from rest_framework.permissions import IsAuthenticated


class ContactListView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):

        contacts = Contact.objects.all()

        serializer = ContactSummarySerializer(contacts, many=True, read_only=True)
        response_data = {
            "contacts": serializer.data
        }

        return Response(response_data, status=status.HTTP_200_OK)