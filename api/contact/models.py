# Django
from django.db import models

from django.core.validators import RegexValidator
from django.utils.translation import ugettext as _

from api.user.models import User
from api.company.models import Company
from api.country.models import Country, City

from django.utils.timezone import now


class Contact(models.Model):
    title = models.CharField(max_length=32, null=False, blank=True)

    first_name = models.CharField(max_length=32, null=False, blank=True)
    last_name = models.CharField(max_length=32, null=False, blank=True)
    email = models.EmailField(blank=False, unique=True)

    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message=_(
        "Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed."))
    phone = models.CharField(max_length=15, validators=[phone_regex], blank=True)

    company = models.ForeignKey(Company, on_delete=models.CASCADE, null=True)
    is_owner = models.BooleanField(default=False)

    country = models.ForeignKey(Country, on_delete=models.SET_NULL, null=True, blank=True)
    city = models.ForeignKey(City, on_delete=models.SET_NULL, null=True, blank=True)

    description = models.TextField(max_length=256)
    is_active = models.BooleanField(default=True)

    insert_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="insert_contact", blank=False)
    insert_date = models.DateTimeField(default=now, editable=False)

    contact_user = models.OneToOneField(User, on_delete=models.CASCADE, null=True, related_name="contact_user")
    
    @staticmethod
    def get_contact(**kwargs):
        """
        @brief    If the contact with the given arguments exists, returns the contact. Otherwise returns None
        """

        contact = None
        try:
            contact = Contact.objects.get(**kwargs)
        except Contact.DoesNotExist:
            pass

        return contact
