# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Contact
from api.contact.models import Contact

# Serializers
from api.contact.serializers import ContactSummarySerializer, ContactUpdateSerializer

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC
from api.field_names import ContactFields
import datetime

from rest_framework.permissions import IsAuthenticated


class ContactDetailView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, contact_id, format=None):
        contact = Contact.get_contact(id=contact_id, is_active=True)
        if contact is not None:
            serializer = ContactSummarySerializer(contact, many=False, read_only=True)
            response_data = {
                "contact": serializer.data
            }

            response = Response(response_data, status=status.HTTP_200_OK)

        else:
            errors = {
                "contact-id": [
                    _("Contact ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A contact with the given ID doesn't exist."), SC.contact_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response


    def delete(self, request, contact_id, format=None):
        contact = Contact.get_contact(id=contact_id, is_active=True)
        if contact is not None:
            serializer = ContactSummarySerializer(contact, many=False, read_only=True)
            response_data = {
                "contact": serializer.data
            }

            contact.is_active = False
            contact.save()
            response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "contact-id": [
                    _("Contact ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A contact with the given ID doesn't exist."), SC.contact_not_exist, errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response


    def put(self, request, contact_id, format=None):
        saved_contact = None
        payload = request.data

        contact = Contact.get_contact(id=contact_id, is_active=True)
        if contact is not None:
            contact_data = self.extract_contact_data(payload, contact)
            if len(contact_data) == 0:
                response = Response(status=status.HTTP_304_NOT_MODIFIED)
            else:
                errors = {}
                contact_serializer = ContactUpdateSerializer(contact, data=contact_data, partial=True, many=False)
                if contact_serializer.is_valid():
                    saved_contact = contact_serializer.save()
                    saved_contact.save()
                else:
                    errors.update(contact_serializer.errors)

                if len(errors) > 0:
                    generated_status = SG.get_status(_("Error occurred while updating the contact."), SC.parameter_error,
                                                     errors)
                    response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)
                else:
                    detail_serializer = ContactSummarySerializer(saved_contact, read_only=True, many=False)
                    response_data = {
                        "contact": detail_serializer.data
                    }
                    response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "contact-id": [
                    _("Contact ID doesn't exist.")
                ]
            }
            generated_status = SG.get_status(_("A contact with the given ID doesn't exist."), SC.contact_not_exist, errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response


    def extract_contact_data(self, payload, contact):
        contact_data = {}

        if ContactFields.title in payload:
            temp_value = payload.pop(ContactFields.title)
            if contact.title != temp_value:
                contact_data[ContactFields.title] = temp_value

        if ContactFields.first_name in payload:
            temp_value = payload.pop(ContactFields.first_name)
            if contact.first_name != temp_value:
                contact_data[ContactFields.first_name] = temp_value

        if ContactFields.last_name in payload:
            temp_value = payload.pop(ContactFields.last_name)
            if contact.last_name != temp_value:
                contact_data[ContactFields.last_name] = temp_value

        if ContactFields.email in payload:
            temp_value = payload.pop(ContactFields.email)
            if contact.email != temp_value:
                contact_data[ContactFields.email] = temp_value

        if ContactFields.phone in payload:
            temp_value = payload.pop(ContactFields.phone)
            if contact.phone != temp_value:
                contact_data[ContactFields.phone] = temp_value

        if ContactFields.company in payload:
            temp_value = payload.pop(ContactFields.company)
            if contact.company != temp_value:
                contact_data[ContactFields.company] = temp_value

        if ContactFields.is_owner in payload:
            temp_value = payload.pop(ContactFields.is_owner)
            if contact.is_owner != temp_value:
                contact_data[ContactFields.is_owner] = temp_value

        if ContactFields.country in payload:
            temp_value = payload.pop(ContactFields.country)
            if contact.country != temp_value:
                contact_data[ContactFields.country] = temp_value

        if ContactFields.city in payload:
            temp_value = payload.pop(ContactFields.city)
            if contact.city != temp_value:
                contact_data[ContactFields.city] = temp_value

        if ContactFields.description in payload:
            temp_value = payload.pop(ContactFields.description)
            if contact.description != temp_value:
                contact_data[ContactFields.description] = temp_value

        if ContactFields.contact_user in payload:
            temp_value = payload.pop(ContactFields.contact_user)
            if contact.contact_user != temp_value:
                contact_data[ContactFields.contact_user] = temp_value

        return contact_data
