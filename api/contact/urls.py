# Django
from django.conf.urls import url

# Session
from api.contact.new import ContactCreateView
from api.contact.detail import ContactDetailView
from api.contact.list import ContactListView

urlpatterns = [
    url(r'^contacts/new/$', ContactCreateView.as_view(), name="contact-new"),
    url(r'^contacts/$', ContactListView.as_view(), name="contact-list"),
    url(r'^contacts/(?P<contact_id>[0-9]+)/$', ContactDetailView.as_view(), name="contact-detail")
]
