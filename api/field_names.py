class _Gender:
    none = 0
    male = 1
    female = 2


class _Relationship:
    none = 0
    single = 1
    married = 2


class _UserStatus:
    none = 0
    banned = 1
    active = 2


class _FinanceStatus:
    active = 0
    passive = 1


class _Currency:
    usd = 1
    tl = 2
    eur = 3


class _LanguageCode:
    tr = 'tr'
    en = 'en'
    fr = 'fr'
    gr = 'gr'
    it = 'it'

class _FinanceType:
    incoming = 1
    outgoing = 2

class _QuantityType:
    gram = 1
    kilogram = 2
    ton = 3


class ParameterOptions:
    gender = _Gender()
    relationship = _Relationship()
    user_status = _UserStatus()

    currency = _Currency()
    language_code = _LanguageCode()

    finance_type = _FinanceType()
    finance_status = _FinanceStatus()

    quantity_type = _QuantityType()


class CommonChoices:
    GENDER_CHOICES = (
        (ParameterOptions.gender.none, 'none'),
        (ParameterOptions.gender.male, 'male'),
        (ParameterOptions.gender.female, 'female')
    )

    LANGUAGE_CODE_CHOICES = (
        (ParameterOptions.language_code.tr, 'tr'),
        (ParameterOptions.language_code.en, 'en'),
        (ParameterOptions.language_code.fr, 'fr'),
        (ParameterOptions.language_code.gr, 'gr'),
        (ParameterOptions.language_code.it, 'it')
    )
    CURRENCY_CHOICES = (
        (ParameterOptions.currency.usd, 1),
        (ParameterOptions.currency.tl, 2),
        (ParameterOptions.currency.eur, 3)
    )

    RELATIONSHIP_CHOICES = (
        (ParameterOptions.relationship.none, 'none'),
        (ParameterOptions.relationship.single, 'single'),
        (ParameterOptions.relationship.married, 'married')
    )

    CUSTOMER_STATUS_CHOICES = (
        (ParameterOptions.user_status.none, 'none'),
        (ParameterOptions.user_status.banned, 'banned'),
        (ParameterOptions.user_status.active, 'active')
    )

    FINANCE_STATUS_CHOICES = (
        (ParameterOptions.finance_status.active, 1),
        (ParameterOptions.finance_status.passive, 0)
    )

    FINANCE_TYPE_CHOICES = (
        (ParameterOptions.finance_type.incoming, 1),
        (ParameterOptions.finance_type.outgoing, 2)
    )

    QUANTITY_TYPE_CHOICES = (
        (ParameterOptions.quantity_type.gram, 1),
        (ParameterOptions.quantity_type.kilogram, 2),
        (ParameterOptions.quantity_type.ton, 3)
    )

    # This list is extracted from here: https://github.com/lukes/ISO-3166-Countries-with-Regional-Codes/blob/master/slim-2/slim-2.json
    COUNTRY_CODE_OPTIONS = (
        ('af', 'af'),
        ('ax', 'ax'),
        ('al', 'al'),
        ('dz', 'dz'),
        ('as', 'as'),
        ('ad', 'ad'),
        ('ao', 'ao'),
        ('ai', 'ai'),
        ('aq', 'aq'),
        ('ag', 'ag'),
        ('ar', 'ar'),
        ('am', 'am'),
        ('aw', 'aw'),
        ('au', 'au'),
        ('at', 'at'),
        ('az', 'az'),
        ('bs', 'bs'),
        ('bh', 'bh'),
        ('bd', 'bd'),
        ('bb', 'bb'),
        ('by', 'by'),
        ('be', 'be'),
        ('bz', 'bz'),
        ('bj', 'bj'),
        ('bm', 'bm'),
        ('bt', 'bt'),
        ('bo', 'bo'),
        ('bq', 'bq'),
        ('ba', 'ba'),
        ('bw', 'bw'),
        ('bv', 'bv'),
        ('br', 'br'),
        ('io', 'io'),
        ('bn', 'bn'),
        ('bg', 'bg'),
        ('bf', 'bf'),
        ('bi', 'bi'),
        ('kh', 'kh'),
        ('cm', 'cm'),
        ('ca', 'ca'),
        ('cv', 'cv'),
        ('ky', 'ky'),
        ('cf', 'cf'),
        ('td', 'td'),
        ('cl', 'cl'),
        ('cn', 'cn'),
        ('cx', 'cx'),
        ('cc', 'cc'),
        ('co', 'co'),
        ('km', 'km'),
        ('cg', 'cg'),
        ('cd', 'cd'),
        ('ck', 'ck'),
        ('cr', 'cr'),
        ('ci', 'ci'),
        ('hr', 'hr'),
        ('cu', 'cu'),
        ('cw', 'cw'),
        ('cy', 'cy'),
        ('cz', 'cz'),
        ('dk', 'dk'),
        ('dj', 'dj'),
        ('dm', 'dm'),
        ('do', 'do'),
        ('ec', 'ec'),
        ('eg', 'eg'),
        ('sv', 'sv'),
        ('gq', 'gq'),
        ('er', 'er'),
        ('ee', 'ee'),
        ('et', 'et'),
        ('fk', 'fk'),
        ('fo', 'fo'),
        ('fj', 'fj'),
        ('fi', 'fi'),
        ('fr', 'fr'),
        ('gf', 'gf'),
        ('pf', 'pf'),
        ('tf', 'tf'),
        ('ga', 'ga'),
        ('gm', 'gm'),
        ('ge', 'ge'),
        ('de', 'de'),
        ('gh', 'gh'),
        ('gi', 'gi'),
        ('gr', 'gr'),
        ('gl', 'gl'),
        ('gd', 'gd'),
        ('gp', 'gp'),
        ('gu', 'gu'),
        ('gt', 'gt'),
        ('gg', 'gg'),
        ('gn', 'gn'),
        ('gw', 'gw'),
        ('gy', 'gy'),
        ('ht', 'ht'),
        ('hm', 'hm'),
        ('va', 'va'),
        ('hn', 'hn'),
        ('hk', 'hk'),
        ('hu', 'hu'),
        ('is', 'is'),
        ('in', 'in'),
        ('id', 'id'),
        ('ir', 'ir'),
        ('iq', 'iq'),
        ('ie', 'ie'),
        ('im', 'im'),
        ('il', 'il'),
        ('it', 'it'),
        ('jm', 'jm'),
        ('jp', 'jp'),
        ('je', 'je'),
        ('jo', 'jo'),
        ('kz', 'kz'),
        ('ke', 'ke'),
        ('ki', 'ki'),
        ('kp', 'kp'),
        ('kr', 'kr'),
        ('kw', 'kw'),
        ('kg', 'kg'),
        ('la', 'la'),
        ('lv', 'lv'),
        ('lb', 'lb'),
        ('ls', 'ls'),
        ('lr', 'lr'),
        ('ly', 'ly'),
        ('li', 'li'),
        ('lt', 'lt'),
        ('lu', 'lu'),
        ('mo', 'mo'),
        ('mk', 'mk'),
        ('mg', 'mg'),
        ('mw', 'mw'),
        ('my', 'my'),
        ('mv', 'mv'),
        ('ml', 'ml'),
        ('mt', 'mt'),
        ('mh', 'mh'),
        ('mq', 'mq'),
        ('mr', 'mr'),
        ('mu', 'mu'),
        ('yt', 'yt'),
        ('mx', 'mx'),
        ('fm', 'fm'),
        ('md', 'md'),
        ('mc', 'mc'),
        ('mn', 'mn'),
        ('me', 'me'),
        ('ms', 'ms'),
        ('ma', 'ma'),
        ('mz', 'mz'),
        ('mm', 'mm'),
        ('na', 'na'),
        ('nr', 'nr'),
        ('np', 'np'),
        ('nl', 'nl'),
        ('nc', 'nc'),
        ('nz', 'nz'),
        ('ni', 'ni'),
        ('ne', 'ne'),
        ('ng', 'ng'),
        ('nu', 'nu'),
        ('nf', 'nf'),
        ('mp', 'mp'),
        ('no', 'no'),
        ('om', 'om'),
        ('pk', 'pk'),
        ('pw', 'pw'),
        ('ps', 'ps'),
        ('pa', 'pa'),
        ('pg', 'pg'),
        ('py', 'py'),
        ('pe', 'pe'),
        ('ph', 'ph'),
        ('pn', 'pn'),
        ('pl', 'pl'),
        ('pt', 'pt'),
        ('pr', 'pr'),
        ('qa', 'qa'),
        ('re', 're'),
        ('ro', 'ro'),
        ('ru', 'ru'),
        ('rw', 'rw'),
        ('bl', 'bl'),
        ('sh', 'sh'),
        ('kn', 'kn'),
        ('lc', 'lc'),
        ('mf', 'mf'),
        ('pm', 'pm'),
        ('vc', 'vc'),
        ('ws', 'ws'),
        ('sm', 'sm'),
        ('st', 'st'),
        ('sa', 'sa'),
        ('sn', 'sn'),
        ('rs', 'rs'),
        ('sc', 'sc'),
        ('sl', 'sl'),
        ('sg', 'sg'),
        ('sx', 'sx'),
        ('sk', 'sk'),
        ('si', 'si'),
        ('sb', 'sb'),
        ('so', 'so'),
        ('za', 'za'),
        ('gs', 'gs'),
        ('ss', 'ss'),
        ('es', 'es'),
        ('lk', 'lk'),
        ('sd', 'sd'),
        ('sr', 'sr'),
        ('sj', 'sj'),
        ('sz', 'sz'),
        ('se', 'se'),
        ('ch', 'ch'),
        ('sy', 'sy'),
        ('tw', 'tw'),
        ('tj', 'tj'),
        ('tz', 'tz'),
        ('th', 'th'),
        ('tl', 'tl'),
        ('tg', 'tg'),
        ('tk', 'tk'),
        ('to', 'to'),
        ('tt', 'tt'),
        ('tn', 'tn'),
        ('tr', 'tr'),
        ('tm', 'tm'),
        ('tc', 'tc'),
        ('tv', 'tv'),
        ('ug', 'ug'),
        ('ua', 'ua'),
        ('ae', 'ae'),
        ('gb', 'gb'),
        ('us', 'us'),
        ('um', 'um'),
        ('uy', 'uy'),
        ('uz', 'uz'),
        ('vu', 'vu'),
        ('ve', 've'),
        ('vn', 'vn'),
        ('vg', 'vg'),
        ('vi', 'vi'),
        ('wf', 'wf'),
        ('eh', 'eh'),
        ('ye', 'ye'),
        ('zm', 'zm'),
        ('zw', 'zw')
    )


class CashFlowFields:
    cash_flow_id = 'id'
    cash_type = 'cash_type'
    description = 'description'
    flow_date = 'flow_date'

    company = 'company'
    contact = 'contact'

    price = 'price'
    currency = 'currency'

    invoice = 'invoice'
    receipt = 'receipt'
    expensive = 'expensive'

    insert_user = 'insert_user'
    insert_date = 'insert_date'


class CompanyFields:
    company_id = 'id'
    name = "name"
    phone = "phone"
    fax = "fax"
    email = "email"
    tax_office = "tax_office"
    tax_no = "tax_no"
    banking_accounts = "banking_accounts"
    description = "description"
    is_active = "is_active"
    country = "country"
    city = "city"
    insert_user = "insert_user"
    insert_date = "insert_date"


class ContactFields:
    contact_id = 'id'
    title = 'title'
    first_name = 'first_name'
    last_name = 'last_name'
    email = 'email'

    phone_regex = 'phone_regex'
    phone = 'phone'

    company = 'company'
    is_owner = 'is_owner'

    country = 'country'
    city = 'city'

    description = 'description'
    is_active = 'is_active'

    insert_user = 'insert_user'
    insert_date = 'insert_date'

    contact_user = 'contact_user'


class DailyPriceFields:
    daily_price_id = 'id'
    daily_date = 'daily_date'
    product = 'product'
    price = 'price'
    currency = 'currency'

    insert_user = 'insert_user'
    insert_date = 'insert_date'


class GeneralFeatureFields:
    feature_id = 'id'
    company = 'company'
    currency = 'currency'
    unit_type = 'unit_type'

    insert_user = 'insert_user'
    insert_date = 'insert_date'


class InvoiceFields:
    invoice_id = 'id'
    invoice_type = 'invoice_type'
    invoice_no = 'invoice_no'

    status = 'status'
    waybills = 'waybills'

    contact = 'contact'
    company = 'company'

    serial_no = 'serial_no'
    order_no = 'order_no'

    price = 'price'
    currency = 'currency'

    description = 'description'
    invoice_url = 'invoice_url'

    invoice_date = 'invoice_date'

    insert_user = 'insert_user'
    insert_date = 'insert_date'

    product = 'product'
    quantity = 'quantity'
    quantity_type = 'quantity_type'

    unit_price = 'unit_price'
    e_invoice = 'e_invoice'


class ProductFields:
    product_id = 'id'
    name = 'name'
    unit_type = 'unit_type'
    description = 'description'

    insert_user = 'insert_user'
    insert_date = 'insert_date'

    is_tree = 'is_tree'
    quantity = 'quantity'


class SubProductFields:
    sub_product_id = 'id'
    product = 'product'
    main_product = 'main_product'

    quantity = 'quantity'
    unit_type = 'unit_type'

    description = 'description'

    insert_user = 'insert_user'
    insert_date = 'insert_date'


class UnitTypeFields:
    unit_type_id = 'id'
    name = 'name'

    main_quantity = 'main_quantity'
    main_unit_type = 'main_unit_type'

    description = 'description'

    insert_user = 'insert_user'
    insert_date = 'insert_date'


class ReceiptFields:
    receipt_id = 'id'
    receipt_no = 'receipt_no'

    status = 'status'
    product = 'product'

    quantity = 'quantity'
    quantity_type = 'quantity_type'

    price = 'price'
    currency = 'price'

    contact = 'contact'
    company = 'company'

    description = 'description'
    receipt_url = 'receipt_url'

    receipt_date = 'receipt_date'

    insert_user = 'insert_user'
    insert_date = 'insert_date'

    store = 'store'
    receipt_type = 'receipt_type'

class SessionFields:
    session_id = 'id'
    user = 'user'
    start_date = 'start_date'
    end_date = 'end_date'
    duration = 'duration'


class StoreFields:
    store_id = 'id'
    name = 'name'
    is_active = 'is_active'

    address = 'address'
    description = 'description'
    store_date = 'store_date'

    insert_user = 'insert_user'
    insert_date = 'insert_date'


class StoreActionFields:
    store_action_id = 'id'
    store = 'store'

    contact = 'contact'
    company = 'company'

    product = 'product'

    action_type = 'action_type'
    action_date = 'action_date'

    quantity = 'quantity'
    quantity_type = 'quantity_type'

    unit_price = 'unit_price'
    currency = 'currency'

    description = 'description'
    waybill = 'waybill'
    receipt = 'receipt'

    insert_user = 'insert_user'
    insert_date = 'insert_date'


class UserFields:
    user_id = 'id'
    username = 'username'

    first_name = 'first_name'
    last_name = 'last_name'

    email = 'email'
    password = 'password'

    is_staff = 'is_staff'
    is_active = 'is_active'

    is_superuser = 'is_superuser'

    last_login = 'last_login'
    date_joined = 'date_joined'


class WaybillFields:
    waybill_id = 'id'
    waybill_type = 'waybill_type'
    waybill_no = 'waybill_no'

    status = 'status'

    contact = 'contact'
    company = 'company'

    serial_no = 'serial_no'
    order_no = 'order_no'

    description = 'description'
    waybill_url = 'waybill_url'

    dispatch_date = 'dispatch_date'
    delivery_date = 'delivery_date'

    insert_user = 'insert_user'
    insert_date = 'insert_date'

    product = 'product'
    quantity = 'quantity'
    quantity_type = 'quantity_type'

    store = 'store'
    is_invoice = 'is_invoice'

class AdminFields:
    admin_id = 'id'
    user = 'user'
    profile_photo = 'profile_photo'

    country = 'country'
    city = 'city'
    language = 'language'

    gender = 'gender'
    phone = 'phone'
    is_supervisor = 'is_supervisor'

    status = 'status'


class BanFields:
    ban_id = 'id'
    user = 'user'
    admin = 'admin'
    ban_date = 'ban_date'

    reason = 'reason'
    end_date = 'end_date'
    is_active = 'is_active'


class CityFields:
    city_id = 'id'
    country = 'country'
    name = 'name'


class CountryTranslationFields:
    tr_id = 'id'
    country = 'country'
    name = 'name'
    language = 'language'


class CountryFields:
    country_id = 'id'
    code = 'code'
    name = 'name'
    language = 'language'


class LanguageFields:
    lang_id = 'id'
    code = 'code'
    name = 'name'
    asd = 'asd'

class ExpensiveFields:
    expensive_id = 'id'
    description = 'description'

    price = 'price'
    currency = 'currency'

    expense_date = 'expense_date'

    insert_user = 'insert_user'
    insert_date = 'insert_date'
