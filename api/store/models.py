# Django
from django.db import models

from api.user.models import User
from api.contact.models import Contact
from api.company.models import Company

from api.product.models import Product
from api.receipt.models import Receipt
from api.waybill.models import Waybill

from api.field_names import CommonChoices

from django.utils.timezone import now

from django.db.models.signals import post_save
from django.dispatch import receiver


class Store(models.Model):
    name = models.CharField(max_length=32, unique=True)
    is_active = models.BooleanField(default=True)

    address = models.TextField(max_length=256)

    description = models.TextField(max_length=256)
    store_date = models.DateTimeField(default=now)

    insert_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="insert_store", blank=False)
    insert_date = models.DateTimeField(default=now, editable=False)

    @staticmethod
    def get_store(**kwargs):
        """
        @brief    If the store with the given arguments exists, returns the store. Otherwise returns None
        """

        store = None
        try:
            store = Store.objects.get(**kwargs)
        except Store.DoesNotExist:
            pass

        return store


class StoreAction(models.Model):
    store = models.ForeignKey(Store, on_delete=models.CASCADE, blank=False)

    contact = models.ForeignKey(Contact, on_delete=models.CASCADE, blank=False)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, null=True)

    product = models.ForeignKey(Product, on_delete=models.CASCADE, blank=False)

    action_type = models.IntegerField(null=False, choices=CommonChoices.FINANCE_TYPE_CHOICES)
    action_date = models.DateTimeField(default=now)

    quantity = models.FloatField(blank=True, null=False)
    quantity_type = models.IntegerField(null=False)

    unit_price = models.FloatField(blank=True, null=True)
    currency = models.IntegerField(null=False, default=2, choices=CommonChoices.CURRENCY_CHOICES)

    description = models.TextField(max_length=256)
    waybill = models.ForeignKey(Waybill, on_delete=models.CASCADE)
    receipt = models.ForeignKey(Receipt, on_delete=models.CASCADE, null=True)

    insert_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="insert_store_action", blank=False)
    insert_date = models.DateTimeField(default=now, editable=False)

    @staticmethod
    def get_store_action(**kwargs):
        """
        @brief    If the action with the given arguments exists, returns the action. Otherwise returns None
        """

        store_action = None
        try:
            store_action = StoreAction.objects.get(**kwargs)
        except StoreAction.DoesNotExist:
            pass

        return store_action

@receiver(post_save, sender=Waybill)
def waybill_post_save_handler(sender, instance, **kwargs):
    try:
        payload = {}
        payload["contact"] = instance.contact
        payload["store"] = instance.store
        payload["company"] = instance.company
        payload["product"] = instance.product
        payload["action_type"] = instance.waybill_type
        payload["action_date"] = instance.delivery_date
        payload["waybill"] = instance
        payload["insert_user"] = instance.insert_user
        payload["insert_date"] = instance.insert_date
        payload["quantity"] = instance.quantity
        payload["quantity_type"] = instance.quantity_type
        payload["description"] = instance.description

        if instance.is_invoice == False:
            if instance.status == 1:
                if instance.waybill_type == 2 :
                    payload["quantity"] = instance.quantity  * -1
            else :
                if instance.waybill_type == 1:
                    payload["quantity"] = instance.quantity  * -1
                payload["description"] = "IPTAL" + instance.description

            StoreAction.objects.create(**payload)
    except Exception as e:
         print("Waybill error", e)

@receiver(post_save, sender=Receipt)
def receipt_post_save_handler(sender, instance, **kwargs):
    try:
        if instance.quantity is not null:
            payload = {}
            payload["contact"] = instance.contact
            payload["store"] = instance.store
            payload["company"] = instance.company
            payload["product"] = instance.product
            payload["action_type"] = instance.receipt_type
            payload["action_date"] = instance.receipt_date
            payload["receipt"] = instance
            payload["insert_user"] = instance.insert_user
            payload["insert_date"] = instance.insert_date

            payload["quantity"] = instance.quantity
            payload["quantity_type"] = instance.quantity_type
            payload["description"] = instance.description

            if instance.status == 1:
                if instance.receipt_type == 2 :
                    payload["quantity"] = instance.quantity  * -1
            else :
                if instance.receipt_type == 1:
                    payload["quantity"] = instance.quantity  * -1
                payload["description"] = "IPTAL" + instance.description

            StoreAction.objects.create(**payload)
    except Exception as e:
        print("Receipt error: ", e)