from rest_framework import serializers

# Store and Actions
from api.store.models import Store, StoreAction

# General
from api.field_names import StoreFields, StoreActionFields

# Serializers
from api.company.serializers import CompanySummarySerializer
from api.contact.serializers import ContactSummarySerializer
from api.product.serializers import ProductSummarySerializer
from api.user.serializers import UserSummarySerializer
from api.receipt.serializers import ReceiptSummarySerializer
from api.waybill.serializers import WaybillSummarySerializer


class StoreCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = (
            StoreFields.name,
            StoreFields.is_active,
            StoreFields.address,
            StoreFields.description,
            StoreFields.store_date,
            StoreFields.insert_user
        )


class StoreSummarySerializer(serializers.ModelSerializer):

    insert_user = UserSummarySerializer(many=False, read_only=True)

    class Meta:
        model = Store
        fields = (
            StoreFields.store_id,
            StoreFields.name,
            StoreFields.is_active,
            StoreFields.address,
            StoreFields.description,
            StoreFields.store_date,
            StoreFields.insert_user,
            StoreFields.insert_date
        )


class StoreUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Store
        fields = (
            StoreFields.name,
            StoreFields.description,
            StoreFields.address,
            StoreFields.store_date
        )


class StoreActionCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = StoreAction
        fields = (
            StoreActionFields.store,

            StoreActionFields.contact,
            StoreActionFields.company,

            StoreActionFields.product,

            StoreActionFields.action_type,
            StoreActionFields.action_date,

            StoreActionFields.quantity,
            StoreActionFields.quantity_type,

            StoreActionFields.unit_price,
            StoreActionFields.currency,

            StoreActionFields.description,
            StoreActionFields.waybill,
            StoreActionFields.receipt,

            StoreActionFields.insert_user,
            StoreActionFields.insert_date
        )


class StoreActionSummarySerializer(serializers.ModelSerializer):

    company =  CompanySummarySerializer(many=False, read_only=True)
    contact = ContactSummarySerializer(many=False, read_only=True)
    product = ProductSummarySerializer(many=False, read_only=True)
    waybill = WaybillSummarySerializer(many=False, read_only=True)
    receipt = ReceiptSummarySerializer(many=False, read_only=True)
    insert_user = UserSummarySerializer(many=False, read_only=True)

    class Meta:
        model = StoreAction
        fields = (
            StoreActionFields.store_action_id,
            StoreActionFields.store,

            StoreActionFields.contact,
            StoreActionFields.company,

            StoreActionFields.product,

            StoreActionFields.action_type,
            StoreActionFields.action_date,

            StoreActionFields.quantity,
            StoreActionFields.quantity_type,

            StoreActionFields.unit_price,
            StoreActionFields.currency,

            StoreActionFields.description,
            StoreActionFields.waybill,
            StoreActionFields.receipt,

            StoreActionFields.insert_user,
            StoreActionFields.insert_date
        )