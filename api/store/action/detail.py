# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Store Action
from api.store.models import StoreAction

# Serializers
from api.store.serializers import StoreActionSummarySerializer

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC

from rest_framework.permissions import IsAuthenticated


class StoreActionDetailView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, store_action_id, format=None):
        store_action = StoreAction.get_store_action(id=store_action_id)
        if store_action is not None:
            serializer = StoreActionSummarySerializer(store_action, many=False, read_only=True)
            response_data = {
                "store-action": serializer.data
            }

            response = Response(response_data, status=status.HTTP_200_OK)

        else:
            errors = {
                "store-action-id": [
                    _("Store Action ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A store action with the given ID doesn't exist."),
                                             SC.store_action_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def delete(self, request, store_action_id, format=None):
        store_action = StoreAction.get_store_action(id=store_action_id)
        if store_action is not None:
            serializer = StoreActionSummarySerializer(store_action, many=False, read_only=True)
            response_data = {
                "store-action": serializer.data
            }

            store_action.delete()
            response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "store-action-id": [
                    _("Store Action ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A store action with the given ID doesn't exist."),
                                             SC.store_action_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response
