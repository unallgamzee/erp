# Django
from django.core.paginator import Paginator

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Store Action
from api.store.serializers import StoreActionSummarySerializer
from api.store.models import StoreAction

# General
from api.field_names import StoreActionFields

from rest_framework.permissions import IsAuthenticated


class StoreActionListView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):
        try:
            store_actions = StoreAction.objects.all()

            serializer = StoreActionSummarySerializer(store_actions, many=True, read_only=True)
            response_data = {
                "store-actions": serializer.data
            }
        
        except Exception as e:
         print(e)

        return Response(response_data, status=status.HTTP_200_OK)