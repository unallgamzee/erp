# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Store
from api.store.models import Store

# Serializers
from api.store.serializers import StoreSummarySerializer, StoreUpdateSerializer

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC
from api.field_names import StoreFields
import datetime

from rest_framework.permissions import IsAuthenticated


class StoreDetailView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, store_id, format=None):
        store = Store.get_store(id=store_id, is_active=True)
        if store is not None:
            serializer = StoreSummarySerializer(store, many=False, read_only=True)
            response_data = {
                "store": serializer.data
            }

            response = Response(response_data, status=status.HTTP_200_OK)

        else:
            errors = {
                "store-id": [
                    _("Store ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A store with the given ID doesn't exist."), SC.store_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def delete(self, request, store_id, format=None):
        store = Store.get_store(id=store_id, is_active=True)
        if store is not None:
            serializer = StoreSummarySerializer(store, many=False, read_only=True)
            response_data = {
                "store": serializer.data
            }

            store.is_active = False
            store.save()
            response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "store-id": [
                    _("Store ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A store with the given ID doesn't exist."), SC.store_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def put(self, request, store_id, format=None):
        saved_store = None
        payload = request.data

        store = Store.get_store(id=store_id, is_active=True)
        if store is not None:
            store_data = self.extract_store_data(payload, store)
            if len(store_data) == 0:
                response = Response(status=status.HTTP_304_NOT_MODIFIED)
            else:
                errors = {}
                serializer = StoreUpdateSerializer(store, data=store_data, partial=True, many=False)
                if serializer.is_valid():
                    saved_store = serializer.save()
                    saved_store.save()
                else:
                    errors.update(serializer.errors)

                if len(errors) > 0:
                    generated_status = SG.get_status(_("Error occurred while updating the store."),
                                                     SC.parameter_error,
                                                     errors)
                    response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)
                else:
                    detail_serializer = StoreSummarySerializer(saved_store, read_only=True, many=False)
                    response_data = {
                        "store": detail_serializer.data
                    }
                    response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "store-id": [
                    _("Store ID doesn't exist.")
                ]
            }
            generated_status = SG.get_status(_("A store with the given ID doesn't exist."), SC.store_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def extract_store_data(self, payload, store):
        store_data = {}

        if StoreFields.name in payload:
            temp_value = payload.pop(StoreFields.name)
            if store.name != temp_value:
                store_data[StoreFields.name] = temp_value

        if StoreFields.store_date in payload:
            temp_value = payload.pop(StoreFields.store_date)
            if store.store_date != temp_value:
                store_data[StoreFields.store_date] = temp_value

        if StoreFields.description in payload:
            temp_value = payload.pop(StoreFields.description)
            if store.description != temp_value:
                store_data[StoreFields.description] = temp_value

        if StoreFields.address in payload:
            temp_value = payload.pop(StoreFields.address)
            if store.address != temp_value:
                store_data[StoreFields.address] = temp_value

        return store_data
