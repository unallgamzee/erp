# Django
from django.core.paginator import Paginator

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Store
from api.store.serializers import StoreSummarySerializer
from api.store.models import Store

# General
from api.field_names import StoreFields

from rest_framework.permissions import IsAuthenticated


class StoreListView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):

        stores = Store.objects.all()

        serializer = StoreSummarySerializer(stores, many=True, read_only=True)
        response_data = {
            "stores": serializer.data
        }

        return Response(response_data, status=status.HTTP_200_OK)