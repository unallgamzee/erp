# Django
from django.conf.urls import url

# Store
from api.store.new import StoreCreateView
from api.store.detail import StoreDetailView
from api.store.list import StoreListView

# Store Action
from api.store.action.new import StoreActionCreateView
from api.store.action.detail import StoreActionDetailView
from api.store.action.list import StoreActionListView

urlpatterns = [
    url(r'^stores/new/$', StoreCreateView.as_view(), name="store-new"),
    url(r'^stores/$', StoreListView.as_view(), name="store-list"),
    url(r'^stores/(?P<store_id>[0-9]+)/$', StoreDetailView.as_view(), name="store-detail"),

    url(r'^store-actions/new/$', StoreActionCreateView.as_view(),
        name="store-product-new"),
    url(r'^store-actions/$', StoreActionListView.as_view(), name="store-product-list"),
    url(r'^store-actions/(?P<store_action_id>[0-9]+)/$', StoreActionDetailView.as_view(),
        name="store-product-detail")
]
