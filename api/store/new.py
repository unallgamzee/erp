# Python
import datetime

# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC

# Store
from api.store.serializers import StoreCreateSerializer, StoreSummarySerializer

from rest_framework.permissions import IsAuthenticated


class StoreCreateView(APIView):

    permission_classes = (IsAuthenticated, )

    def post(self, request, format=None):
        try:
            payload = request.data
            payload['insert_user'] = request.user.id
            serializer = StoreCreateSerializer(data=payload, many=False)

            if serializer.is_valid():
                store = serializer.save()
                detail_serializer = StoreSummarySerializer(store, read_only=True, many=False)
                response_data = {
                    'store': detail_serializer.data
                }
                response = Response(response_data, status=status.HTTP_201_CREATED)
            else:
                generated_status = SG.get_status(_("Parameter error."), SC.parameter_error, serializer.errors)
                response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)
        
        except Exception as e:
            print("Store error :", e)

        return response
