# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC

# Country
from api.country.models import Country
from api.country.serializers import CountryCreateSerializer, CountrySerializer

# Common
from api.field_names import CountryFields


class CountryCreateView(APIView):

    def post(self, request, format=None):
        payload = request.data
        serializer = CountryCreateSerializer(data=payload, many=False)

        if serializer.is_valid():
            country = serializer.save()
            detail_serializer = CountrySerializer(country, read_only=True, many=False)
            response_data = {
                'country': detail_serializer.data
            }
            response = Response(response_data, status=status.HTTP_201_CREATED)
        else:
            generated_status = SG.get_status(_("Parameter error."), SC.parameter_error, serializer.errors)
            response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)

        return response
