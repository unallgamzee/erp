# Django
from django.db import models

# Language
from api.language.models import Language

# Common
from api.field_names import CommonChoices


class Country(models.Model):
    code = models.CharField(max_length=64, unique=True, null=False, blank=False, choices=CommonChoices.COUNTRY_CODE_OPTIONS)
    language = models.ForeignKey(Language, on_delete=models.CASCADE)
    name = models.CharField(max_length=64, null=False, blank=False)

    @staticmethod
    def get_country(**kwargs):
        country = None
        try:
            country = Country.objects.get(**kwargs)
        except Country.DoesNotExist:
            pass

        return country


class City(models.Model):
    name = models.CharField(max_length=64)
    country = models.ForeignKey(Country, on_delete=models.CASCADE, related_name='cities')

    @staticmethod
    def get_city(**kwargs):
        city = None
        try:
            city = City.objects.get(**kwargs)
        except City.DoesNotExist:
            pass

        return city
