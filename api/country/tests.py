# coding=utf-8

# Rest Framework
from rest_framework import status
from rest_framework.test import APITestCase
from rest_framework.test import APIClient

# Python
import json

# Common
from api.field_names import ParameterOptions

# Country
from api.country.models import Country, City, Language


class CountryTests(APITestCase):

    token = ''
    token_insufficent_permissions = ''
    language_id = -1
    country_id = -1
    city_id = -1
    client = APIClient(HTTP_ACCEPT='application/json; version=1.0')

    def runTest(self, token, token_insufficent_permissions):
        self.token = token
        self.token_insufficent_permissions = token_insufficent_permissions

        print("##### Starting Country Tests ----")

        if Language.objects.count() == 0:
            lang = Language(name='English', code='en')
            lang.save()
            self.language_id = lang.id
        else:
            self.language_id = Language.objects.all()[0].id

        print("-- Deleting Countries")
        Country.objects.all().delete()
        print("++ Completed Deleting Countries")

        self.test_create_country()
        self.test_create_country_error()
        self.test_create_city()

        self.test_create_city_error()
        self.test_list_countries()
        self.test_list_cities()

        self.test_delete_city()
        self.test_delete_country()

        print("##### Country Tests Finished ----")

    def test_list_countries(self):
        """
        @brief    Ensure we can list the countries
        """

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        url = '/countries/'

        print("-- Starting country list tests")

        response = self.client.get(url, format='json')
        response_dict = json.loads(response.content.decode("utf-8"))

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response_dict)

        print("++ Country list test finished!")

    def test_create_country(self):
        """
        @brief    Ensure we can create a country
        """

        data = {
            'name': 'United States',
            'code': 'us',
            'language': self.language_id
        }

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        url = '/countries/new/'
        previous_country_count = Country.objects.count()

        print("-- Starting country create tests")

        response = self.client.post(url, data, format='json')
        response_dict = json.loads(response.content.decode("utf-8"))
        current_country_count = Country.objects.count()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED, msg=response_dict)
        self.assertEqual(current_country_count, previous_country_count + 1, msg=response_dict)
        self.country_id = response_dict.get('country').get('id')

        print("++ Country create test finished!")

    def test_create_country_error(self):
        """
        @brief    Ensure we can create a country
        """

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_insufficent_permissions)
        url = '/countries/new/'

        print("-- Starting country create error tests")

        previous_country_count = Country.objects.count()

        data = {
            'name': 'United States',
            'code': 'us',
            'language': self.language_id
        }

        response = self.client.post(url, data, format='json')
        response_dict = json.loads(response.content.decode("utf-8"))
        current_country_count = Country.objects.count()

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, msg=response_dict)
        self.assertEqual(previous_country_count, current_country_count, msg=response_dict)

        print("++ Country create error tests finished!")

    def test_delete_country(self):
        """
        @brief    Ensure we can delete a country
        """

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        url = '/countries/%s/' % (self.country_id)

        print("-- Starting country delete tests")
        previous_country_count = Country.objects.count()

        response = self.client.delete(url, format='json')
        current_country_count = Country.objects.count()

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg="Cannot delete country.")
        self.assertEqual(current_country_count, previous_country_count - 1, msg="Country count was not reduced.")

        print("++ Country delete test finished!")

    def test_list_cities(self):
        """
        @brief    Ensure we can list the cities
        """

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        url = '/countries/%s/cities/' % (self.country_id)

        print("-- Starting city list tests")

        response = self.client.get(url, format='json')
        response_dict = json.loads(response.content.decode("utf-8"))

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg=response_dict)

        print("++ City list test finished!")

    def test_create_city(self):
        """
        @brief    Ensure we can create a city
        """

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        url = '/countries/%s/cities/new/' % (self.country_id)

        print("-- Starting city create tests")
        data = {
            'name': 'New York City'
        }
        previous_city_count = City.objects.count()

        response = self.client.post(url, data, format='json')
        response_dict = json.loads(response.content.decode("utf-8"))
        current_city_count = City.objects.count()

        self.assertEqual(response.status_code, status.HTTP_201_CREATED, msg=response_dict)
        self.assertEqual(current_city_count, previous_city_count + 1, msg=response_dict)
        self.city_id = response_dict.get('city').get('id')

        print("++ City create test finished!")

    def test_create_city_error(self):
        """
        @brief    Ensure we can create a city
        """

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token_insufficent_permissions)
        url = '/countries/%s/cities/new/' % (self.country_id)

        print("-- Starting city create error tests")

        data = {
            'name': 'Ankara'
        }
        previous_city_count = City.objects.count()

        response = self.client.post(url, data, format='json')
        response_dict = json.loads(response.content.decode("utf-8"))
        current_city_count = City.objects.count()

        self.assertEqual(response.status_code, status.HTTP_403_FORBIDDEN, msg=response_dict)
        self.assertEqual(current_city_count, previous_city_count, msg=response_dict)

        print("++ City create error test finished!")

    def test_delete_city(self):
        """
        @brief    Ensure we can delete a city
        """

        self.client.credentials(HTTP_AUTHORIZATION='JWT ' + self.token)
        url = '/countries/cities/%s/' % (self.city_id)

        print("-- Starting city delete tests")
        previous_city_count = City.objects.count()

        response = self.client.delete(url, format='json')
        current_city_count = City.objects.count()

        self.assertEqual(response.status_code, status.HTTP_200_OK, msg="Cannot delete the city")
        self.assertEqual(current_city_count, previous_city_count - 1, msg="City count was not reduced")

        print("++ City delete test finished!")
