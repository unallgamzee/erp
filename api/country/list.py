# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Country
from api.country.models import Country
from api.country.serializers import CountrySerializer


class CountryListView(APIView):

    def get(self, request, format=None):
        countries = Country.objects.all()
        serializer = CountrySerializer(countries, many=True, read_only=True)
        payload = {
            'countries': serializer.data
        }

        return Response(payload, status=status.HTTP_200_OK)
