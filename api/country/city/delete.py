# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC

# Country
from api.country.models import City
from api.country.serializers import CitySerializer


class CityDeleteView(APIView):

    def delete(self, request, city_id, format=None):
        response = None
        city = City.get_city(pk=city_id)

        if city is None:
            errors = {
                'city-id': [_("A city with this ID doesn't exist")]
            }
            generated_status = SG.get_status(_("City doesn't exist."), SC.city_no_exist, errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)
        else:
            serializer = CitySerializer(city, read_only=True)
            response_data = {
                'city': serializer.data
            }

            city.delete()
            response = Response(response_data, status=status.HTTP_200_OK)

        return response
