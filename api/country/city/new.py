# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC
from api.field_names import CityFields

# Country
from api.country.models import Country
from api.country.serializers import CitySerializer, CityCreateSerializer


class CityCreateView(APIView):

    def post(self, request, country_id, format=None):
        payload = request.data
        response = None
        # First check If we have the country with the given code
        country = Country.get_country(pk=country_id)
        if country is None:
            errors = {
                'country': [_("A country with this code doesn't exist.")]
            }
            generated_status = SG.get_status(_("Country doesn't exist."), SC.country_no_exist, errors)
            response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)
        else:
            payload[CityFields.country] = country.id
            serializer = CityCreateSerializer(data=payload)
            if serializer.is_valid():
                city = serializer.save()
                detail_serializer = CitySerializer(city, read_only=True, many=False)
                response_data = {
                    'city': detail_serializer.data
                }
                response = Response(response_data, status=status.HTTP_201_CREATED)
            else:
                generated_status = SG.get_status(_("Parameter error."), SC.parameter_error, serializer.errors)
                response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)

        return response
