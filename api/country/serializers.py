# REST Framework
from rest_framework import serializers

# Country
from api.country.models import Country, City

# Common
from api.field_names import CountryFields, CityFields

# Language
from api.language.serializers import LanguageSerializer


class CountrySerializer(serializers.ModelSerializer):
    """
    @brief    This is used for listing
    """

    class Meta:
        model = Country
        fields = (
            CountryFields.country_id,
            CountryFields.code,
            CountryFields.language,
            CountryFields.name
        )


class CountryCreateSerializer(serializers.ModelSerializer):
    """
    @brief    This is used for creating a country.
    @swg_begin
    definition: CountryCreate
    type: object
    description: The model to use when creating a country
    properties:
      code:
        format: 'char[3]'
        type: string
        example: us
        description: >
          The ISO 3166-1 alpha-2 is used to determine the countries (eg: TR, US).
          This way, the API can return translations of the country names according to the user's language or the `Accept-Language` header.
          For the list of the codes see [here](https://en.wikipedia.org/wiki/ISO_3166-1_alpha-2)
      name:
        format: 'char[64]'
        type: string
        example: United States
        description: The name of the country in the requested language
      language:
        $ref: '#/definitions/Language'
    @swg_end
    """

    class Meta:
        model = Country
        fields = (
            CountryFields.code,
            CountryFields.language,
            CountryFields.name
        )


class CitySerializer(serializers.ModelSerializer):
    """
    @brief    This is used for listing
    """

    country = CountrySerializer(many=False, read_only=True)

    class Meta:
        model = City
        fields = (
            CityFields.city_id,
            CityFields.name,
            CityFields.country
        )


class CityCreateSerializer(serializers.ModelSerializer):
    """
    @brief    This is used for creating a city
    """

    class Meta:
        model = City
        fields = (
            CityFields.name,
            CityFields.country
        )
