# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC

# Country
from api.country.models import Country
from api.country.serializers import CountrySerializer


class CountryDeleteView(APIView):

    def delete(self, request, country_id, format=None):
        response = None
        country = Country.get_country(pk=country_id)

        if country is None:
            errors = {
                'country-id': [_("A country with this ID doesn't exist")]
            }
            generated_status = SG.get_status(_("Country doesn't exist."), SC.country_no_exist, errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)
        else:
            serializer = CountrySerializer(country, read_only=True)
            response_data = {
                'country': serializer.data
            }
            country.delete()
            response = Response(response_data, status=status.HTTP_200_OK)

        return response
