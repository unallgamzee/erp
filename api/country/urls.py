# Django
from django.conf.urls import url

# Country
from api.country.list import CountryListView
from api.country.new import CountryCreateView
from api.country.delete import CountryDeleteView

# City
from api.country.city.list import CityListView
from api.country.city.new import CityCreateView
from api.country.city.delete import CityDeleteView

urlpatterns = [
    # Country
    url(r'^countries/$', CountryListView.as_view(), name="country-list"),
    url(r'^countries/new/$', CountryCreateView.as_view(), name="country-create"),
    url(r'^countries/(?P<country_id>[0-9]+)/$', CountryDeleteView.as_view(), name="country-delete"),
    # City
    url(r'^countries/(?P<country_id>[0-9]+)/cities/$', CityListView.as_view(), name="city-list"),
    url(r'^countries/(?P<country_id>[0-9]+)/cities/new/$', CityCreateView.as_view(), name="city-create"),
    url(r'^countries/cities/(?P<city_id>[0-9]+)/$', CityDeleteView.as_view(), name="city-delete")
]
