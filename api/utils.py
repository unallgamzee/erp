# JSON Web Token
import jwt
from rest_framework_jwt.utils import jwt_payload_handler
import erp.settings


class ApiUtils:

    @staticmethod
    def create_token(user):
        """
        @brief    Creates a token for the given user.
        @param    user
        @return   Returns the token string.
        """

        payload = jwt_payload_handler(user)
        token = jwt.encode(payload, erp.settings.SECRET_KEY)

        return token.decode('unicode_escape')