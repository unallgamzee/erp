# Django
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from rest_framework.permissions import AllowAny

# User
from api.user.models import User
from api.user.serializers import UserCreateSerializer

# Common
from api.utils import ApiUtils
from api.field_names import UserFields
from api.status_generator import StatusGenerator as SG, StatusCodes as SC

# User
from api.user.models import User


class Login(APIView):
    permission_classes = (AllowAny, )

    def post(self, request, format=None):
        payload = request.data
        response = None
        errors = {}

        username = payload.get(UserFields.username)
        password = payload.get(UserFields.password)
        if username is None:
            errors["username"] = "This field is required."
        if password is None:
            errors["password"] = "This field is required."

        if len(errors) > 0:
            generated_response = SG.get_status("Parameter error.", SC.parameter_error, errors)
            response = Response(generated_response, status=status.HTTP_400_BAD_REQUEST)

        else:
            # Check If the user exists
            user = authenticate(request, username=username, password=password)
            if user is not None:
                token = ApiUtils.create_token(user)
                response_data = {
                    'token': token
                }
                response = Response(response_data, status=status.HTTP_200_OK)
            else:
                errors = {'username': ["User does not exist!"]}
                generated_response = SG.get_status(username, SC.user_not_exist, errors)
                response = Response(generated_response, status=status.HTTP_404_NOT_FOUND)

        return response