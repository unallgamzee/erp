# Django
from django.conf.urls import url

# Product
from api.product.new import ProductCreateView
from api.product.list import ProductListView
from api.product.detail import ProductDetailView

urlpatterns = [
    url(r'^products/new/$', ProductCreateView.as_view(), name="product-new"),
    url(r'^products/$', ProductListView.as_view(), name="product-list"),
    url(r'^products/(?P<product_id>[0-9]+)/$', ProductDetailView.as_view(), name="product-detail")
]
