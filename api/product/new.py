# Python
import datetime

# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC

# product
from api.product.serializers import ProductCreateSerializer, ProductSummarySerializer

from rest_framework.permissions import IsAuthenticated


class ProductCreateView(APIView):

    permission_classes = (IsAuthenticated, )

    def post(self, request, format=None):
        payload = request.data
        payload['insert_user'] = request.user.id
        serializer = ProductCreateSerializer(data=payload, many=False)

        if serializer.is_valid():
            product = serializer.save()
            detail_serializer = ProductSummarySerializer(product, read_only=True, many=False)
            response_data = {
                'product': detail_serializer.data
            }
            response = Response(response_data, status=status.HTTP_201_CREATED)
        else:
            generated_status = SG.get_status(_("Parameter error."), SC.parameter_error, serializer.errors)
            response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)

        return response
