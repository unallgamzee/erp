# Django
from django.core.paginator import Paginator

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Product
from api.product.serializers import ProductSummarySerializer
from api.product.models import Product

# General
from api.field_names import ProductFields

from rest_framework.permissions import IsAuthenticated


class ProductListView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):
        products = Product.objects.all()
        serializer = ProductSummarySerializer(products, many=True, read_only=True)

        response_data = {
            "products": serializer.data
        }

        return Response(response_data, status=status.HTTP_200_OK)