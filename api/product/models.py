# Django
from django.db import models

from api.user.models import User

from django.utils.timezone import now

from api.field_names import CommonChoices


class Product(models.Model):
    name = models.CharField(max_length=128, unique=True, null=False, blank=True)    
    description = models.TextField()

    insert_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="insert_product", blank=False)
    insert_date = models.DateTimeField(default=now, editable=False)
    is_tree = models.BooleanField(default=False)

    @staticmethod
    def get_product(**kwargs):
        """
        @brief    If the product with the given arguments exists, returns the product. Otherwise returns None
        """

        product = None
        try:
            product = Product.objects.get(**kwargs)
        except Product.DoesNotExist:
            pass
            
        return product
