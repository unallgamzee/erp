# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Product
from api.product.models import Product

# Serializers
from api.product.serializers import ProductSummarySerializer, ProductUpdateSerializer

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC
from api.field_names import ProductFields
import datetime

from rest_framework.permissions import IsAuthenticated


class ProductDetailView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, product_id, format=None):
        product = Product.get_product(id=product_id)
        if product is not None:
            serializer = ProductSummarySerializer(product, many=False, read_only=True)
            response_data = {
                "product": serializer.data
            }

            response = Response(response_data, status=status.HTTP_200_OK)

        else:
            errors = {
                "product-id": [
                    _("Product ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A product with the given ID doesn't exist."), SC.product_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def delete(self, request, product_id, format=None):
        product = Product.get_product(id=product_id)
        if product is not None:
            serializer = ProductSummarySerializer(product, many=False, read_only=True)
            response_data = {
                "product": serializer.data
            }

            product.delete()
            response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "product-id": [
                    _("Product ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A product with the given ID doesn't exist."), SC.product_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def put(self, request, product_id, format=None):
        saved_product = None
        payload = request.data

        product = Product.get_product(id=product_id)
        if product is not None:
            product_data = self.extract_product_data(payload, product)
            if len(product_data) == 0:
                response = Response(status=status.HTTP_304_NOT_MODIFIED)
            else:
                errors = {}
                product_serializer = ProductUpdateSerializer(product, data=product_data, partial=True, many=False)
                if product_serializer.is_valid():
                    saved_product = product_serializer.save()
                    saved_product.save()
                else:
                    errors.update(product_serializer.errors)

                if len(errors) > 0:
                    generated_status = SG.get_status(_("Error occurred while updating the product."),
                                                     SC.parameter_error,
                                                     errors)
                    response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)
                else:
                    detail_serializer = ProductSummarySerializer(saved_product, read_only=True, many=False)
                    response_data = {
                        "product": detail_serializer.data
                    }
                    response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "product-id": [
                    _("Product ID doesn't exist.")
                ]
            }
            generated_status = SG.get_status(_("A product with the given ID doesn't exist."), SC.product_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def extract_product_data(self, payload, product):
        product_data = {}

        if ProductFields.name in payload:
            temp_value = payload.pop(ProductFields.name)
            if product.name != temp_value:
                product_data[ProductFields.name] = temp_value

        if ProductFields.description in payload:
            temp_value = payload.pop(ProductFields.description)
            if product.description != temp_value:
                product_data[ProductFields.description] = temp_value

        if ProductFields.is_tree in payload:
            temp_value = payload.pop(ProductFields.is_tree)
            if product.is_tree != temp_value:
                product_data[ProductFields.is_tree] = temp_value

        return product_data
