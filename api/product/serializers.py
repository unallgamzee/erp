from rest_framework import serializers

# Product
from api.product.models import Product

# General
from api.field_names import ProductFields

# Serializers
from api.user.serializers import UserSummarySerializer


class ProductCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = (
            ProductFields.name,
            ProductFields.description,

            ProductFields.insert_user,
            ProductFields.insert_date,

            ProductFields.is_tree
        )


class ProductSummarySerializer(serializers.ModelSerializer):

    insert_user = UserSummarySerializer(many=False, read_only=True)

    class Meta:
        model = Product
        fields = (
            ProductFields.product_id,
            ProductFields.name,

            ProductFields.description,

            ProductFields.insert_user,
            ProductFields.insert_date,

            ProductFields.is_tree
        )


class ProductUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = (
            ProductFields.name,
            ProductFields.description,

            ProductFields.is_tree
        )
