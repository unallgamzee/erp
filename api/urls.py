# Python
import os

# Django
from django.conf.urls import url, include

# Rest Framework
from rest_framework.urlpatterns import format_suffix_patterns

# Rest Framework JWT
from rest_framework_jwt.views import refresh_jwt_token

# Login
from api.login import Login


urlpatterns = [
    url(r'^token-refresh/', refresh_jwt_token),
    url(r'^login/', Login.as_view()),
    url(r'^', include('api.user.urls')),
    url(r'^', include('api.language.urls')),
    url(r'^', include('api.country.urls')),
    url(r'^', include('api.session.urls')),
    url(r'^', include('api.company.urls')),
    url(r'^', include('api.contact.urls')),
    url(r'^', include('api.product.urls')),
    url(r'^', include('api.invoice.urls')),
    url(r'^', include('api.waybill.urls')),
    url(r'^', include('api.receipt.urls')),
    url(r'^', include('api.store.urls')),
    url(r'^', include('api.cash_flow.urls')),
    url(r'^', include('api.general_feature.urls')),
    url(r'^', include('api.daily_price.urls')),
    url(r'^', include('api.expensive.urls'))
]

urlpatterns = format_suffix_patterns(urlpatterns)
