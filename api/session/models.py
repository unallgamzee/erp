from django.db import models
from api.models import User


class Session(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
    start_date = models.DateTimeField(null=False, blank=False)
    end_date = models.DateTimeField(null=True)
    duration = models.FloatField(null=True, help_text="Represents the session duration in minutes")

    @staticmethod
    def get_session(**kwargs):
        """
        @brief    If the session with the given arguments exists, returns the session. Otherwise returns None
        """

        session = None
        try:
            session = Session.objects.get(**kwargs)
        except Session.DoesNotExist:
            pass

        return session
