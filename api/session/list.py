from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status
from django.db.models import Q

# Session
from api.session.models import Session
from api.session.serializers import SessionDetailSerializer

# General
from django.core.paginator import Paginator

from rest_framework.permissions import IsAuthenticated


class SessionList(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):

        sessions = None
        sessions = Session.objects.all()

        serializer = SessionDetailSerializer(sessions, many=True, read_only=True)
        response = {
            'sessions': serializer.data
        }
        return Response(response, status=status.HTTP_200_OK)