from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Session
from api.session.serializers import SessionCreateSerializer, SessionDetailSerializer

# General
from api.field_names import SessionFields
from api.status_generator import StatusGenerator as SG, StatusCodes as SC

from rest_framework.permissions import IsAuthenticated


class SessionNew(APIView):

    permission_classes = (IsAuthenticated, )

    def post(self, request, format=None):

        auth_user = request.user
        data = request.data
        # If the user does not exist in the data, set the authenticated user to it
        if data.get(SessionFields.user) is None:
            data[SessionFields.user] = auth_user.id

        session_serializer = SessionCreateSerializer(data=data)
        if session_serializer.is_valid():
            session = session_serializer.save()

            serializer = SessionDetailSerializer(session, many=False, read_only=True)
            return Response({'session': serializer.data}, status=status.HTTP_201_CREATED)

        generated_response = SG.get_status("Parameter error occurred.", SC.parameter_error, serializer.errors)
        return Response(generated_response, status=status.HTTP_400_BAD_REQUEST)