from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Session
from api.session.serializers import SessionDetailSerializer, SessionUpdateSerializer

# General
from api.field_names import SessionFields
from api.status_generator import StatusGenerator as SG, StatusCodes as SC
from api.session.utils import SessionUtils

from rest_framework.permissions import IsAuthenticated


class SessionDetail(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, session_id, format=None):
        session = SessionUtils.get_session(pk=session_id)
        if session is None:
            return SessionUtils.get_session_not_exist_error(session_id)

        serializer = SessionDetailSerializer(session, many=False, read_only=True)
        return Response({"session": serializer.data}, status=status.HTTP_200_OK)

    def put(self, request, session_id, format=None):

        generated_status = None
        response = None

        if request.data.get(SessionFields.end_date) is None:
            errors = {SessionFields.end_date: ['This field is required.']}
            generated_status = SG.get_status("Parameter error occurred.", SC.parameter_error, errors)
            response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)

        session = SessionUtils.get_session(pk=session_id)
        if session is None:
            response = SessionUtils.get_session_not_exist_error(session_id)

        session_serializer = SessionUpdateSerializer(session, data=request.data, partial=True)
        if session_serializer.is_valid():
            session = session_serializer.save()
            delta = session.end_date - session.start_date
            # Duration is in minutes
            session.duration = ((delta.days * 24) * 60) + (delta.seconds / 60.0)
            session.save()

            serializer = SessionDetailSerializer(session, many=False, read_only=True)
            response = Response({'session': serializer.data}, status=status.HTTP_200_OK)
        else:
            generated_status = SG.get_status("Parameter error occurred.", SC.paremeter_error, serializer.errors)
            response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)

        return response
