from django.conf.urls import url

# Session
from api.session.new import SessionNew
from api.session.list import SessionList
from api.session.detail import SessionDetail

urlpatterns = [
    url(r'^sessions/new/$', SessionNew.as_view(), name="session-new"),
    url(r'^sessions/$', SessionList.as_view(), name="session-list"),
    url(r'^sessions/(?P<session_id>[0-9]+)/$', SessionDetail.as_view(), name="session-detail")
]