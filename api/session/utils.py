from rest_framework.response import Response
from rest_framework import status

from api.session.models import Session
from api.status_generator import StatusGenerator as SG, StatusCodes as SC


class SessionUtils:

    @staticmethod
    def get_session(**kwargs):
        session = None
        try:
            session = Session.objects.get(**kwargs)
        except Session.DoesNotExist:
            pass

        return session

    @staticmethod
    def get_session_not_exist_error(session_id):
        errors = {'id': ["Session does not exist!"]}
        generated_response = SG.get_status(session_id, SC.session_not_exist, errors)
        return Response(generated_response, status=status.HTTP_404_NOT_FOUND)