from rest_framework import serializers

from api.session.models import Session
from api.field_names import SessionFields


class SessionCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Session
        fields = (
            SessionFields.user,
            SessionFields.start_date,
            SessionFields.end_date
        )


class SessionUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Session
        fields = (
            SessionFields.end_date
        )


class SessionDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Session
        fields = (
            SessionFields.session_id,
            SessionFields.user,
            SessionFields.start_date,
            SessionFields.end_date,
            SessionFields.duration
        )
