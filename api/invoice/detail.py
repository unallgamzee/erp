# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Invoice
from api.invoice.models import Invoice

# Serializers
from api.invoice.serializers import InvoiceSummarySerializer

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC
from api.field_names import InvoiceFields
import datetime

from rest_framework.permissions import IsAuthenticated


class InvoiceDetailView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, invoice_id, format=None):
        invoice = Invoice.get_invoice(id=invoice_id, status=1)
        if invoice is not None:
            serializer = InvoiceSummarySerializer(invoice, many=False, read_only=True)
            response_data = {
                "invoice": serializer.data
            }

            response = Response(response_data, status=status.HTTP_200_OK)

        else:
            errors = {
                "invoice-id": [
                    _("Invoice ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A invoice with the given ID doesn't exist."), SC.invoice_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def delete(self, request, invoice_id, format=None):
        invoice = Invoice.get_invoice(id=invoice_id, status=1)
        if invoice is not None:
            invoice.status = 0
            invoice.save()

            serializer = InvoiceSummarySerializer(invoice, many=False, read_only=True)
            response_data = {
                "invoice": serializer.data
            }
            response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "invoice-id": [
                    _("Invoice ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A invoice with the given ID doesn't exist."), SC.invoice_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response
