# Python
import datetime

# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC

# Invoice
from api.invoice.serializers import InvoiceCreateSerializer, InvoiceSummarySerializer

# Waybill
from api.waybill.models import Waybill

from rest_framework.permissions import IsAuthenticated


class InvoiceCreateView(APIView):

    permission_classes = (IsAuthenticated, )

    def post(self, request, format=None):
        try:
            payload = request.data
            payload['insert_user'] = request.user.id
            serializer = InvoiceCreateSerializer(data=payload, many=False)

            if serializer.is_valid():
                invoice = serializer.save()
                
                # waybills operations
                for item in payload['waybills']:
                    waybill = Waybill.get_waybill(id=item, status=1)
                    waybill.is_invoice = True
                    waybill.save()

                detail_serializer = InvoiceSummarySerializer(invoice, read_only=True, many=False)
                response_data = {
                    'invoice': detail_serializer.data
                }
                response = Response(response_data, status=status.HTTP_201_CREATED)
            else:
                generated_status = SG.get_status(_("Parameter error."), SC.parameter_error, serializer.errors)
                response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)

        except Exception as e:
            print("Invoice post error :", e)

        return response
