# Django
from django.core.paginator import Paginator

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Invoice
from api.invoice.serializers import InvoiceSummarySerializer
from api.invoice.models import Invoice

# General
from api.field_names import InvoiceFields

from rest_framework.permissions import IsAuthenticated


class InvoiceListView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):
        try:
            invoices = Invoice.objects.all()

            serializer = InvoiceSummarySerializer(invoices, many=True, read_only=True)
            response_data = {
                "invoices": serializer.data
            }

        except Exception as e:
            print("Invoice list error :", e)

        return Response(response_data, status=status.HTTP_200_OK)