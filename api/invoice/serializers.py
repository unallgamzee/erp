from rest_framework import serializers

# User
from api.invoice.models import Invoice

# General
from api.field_names import InvoiceFields

# Serializers
from api.company.serializers import CompanySummarySerializer
from api.contact.serializers import ContactSummarySerializer
from api.user.serializers import UserSummarySerializer
from api.waybill.serializers import WaybillSummarySerializer


class InvoiceCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Invoice
        fields = (
            InvoiceFields.invoice_type,
            InvoiceFields.invoice_no,

            InvoiceFields.status,
            InvoiceFields.waybills,

            InvoiceFields.contact,
            InvoiceFields.company,

            InvoiceFields.serial_no,
            InvoiceFields.order_no,

            InvoiceFields.price,
            InvoiceFields.currency,

            InvoiceFields.description,
            InvoiceFields.invoice_url,

            InvoiceFields.invoice_date,

            InvoiceFields.insert_user,
            InvoiceFields.insert_date,

            InvoiceFields.product,

            InvoiceFields.quantity,
            InvoiceFields.quantity_type,
            InvoiceFields.unit_price,
            InvoiceFields.e_invoice
        )


class InvoiceSummarySerializer(serializers.ModelSerializer):

    waybills = WaybillSummarySerializer(many=True, read_only=True)
    company =  CompanySummarySerializer(many=False, read_only=True)
    contact = ContactSummarySerializer(many=False, read_only=True)
    insert_user = UserSummarySerializer(many=False, read_only=True)


    class Meta:
        model = Invoice
        fields = (
            InvoiceFields.invoice_id,
            InvoiceFields.invoice_type,
            InvoiceFields.invoice_no,

            InvoiceFields.status,
            InvoiceFields.waybills,

            InvoiceFields.company,
            InvoiceFields.contact,

            InvoiceFields.serial_no,
            InvoiceFields.order_no,

            InvoiceFields.price,
            InvoiceFields.currency,

            InvoiceFields.description,
            InvoiceFields.invoice_url,

            InvoiceFields.invoice_date,

            InvoiceFields.insert_user,
            InvoiceFields.insert_date,
            InvoiceFields.product,

            InvoiceFields.quantity,
            InvoiceFields.quantity_type,
            InvoiceFields.unit_price,
            InvoiceFields.e_invoice
        )