# Django
from django.conf.urls import url

# Invoice
from api.invoice.new import InvoiceCreateView
from api.invoice.detail import InvoiceDetailView
from api.invoice.list import InvoiceListView

urlpatterns = [
    url(r'^invoices/new/$', InvoiceCreateView.as_view(), name="invoice-new"),
    url(r'^invoices/$', InvoiceListView.as_view(), name="invoice-list"),
    url(r'^invoices/(?P<invoice_id>[0-9]+)/$', InvoiceDetailView.as_view(), name="invoice-detail")
]
