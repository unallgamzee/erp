# Django
from django.db import models

from api.user.models import User
from api.company.models import Company
from api.contact.models import Contact

from api.product.models import Product
from api.waybill.models import Waybill

from django.utils.timezone import now

from api.field_names import CommonChoices


class Invoice(models.Model):
    invoice_type = models.IntegerField(null=False, choices=CommonChoices.FINANCE_TYPE_CHOICES)
    invoice_no = models.CharField(max_length=32, unique=True, null=False, blank=True)
    status = models.IntegerField(null=False, choices=CommonChoices.FINANCE_STATUS_CHOICES)

    waybills = models.ManyToManyField(Waybill, related_name='invoices', blank=True)

    contact = models.ForeignKey(Contact, on_delete=models.CASCADE, blank=False)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, blank=False)

    serial_no = models.CharField(max_length=32, null=False, blank=True)
    order_no = models.CharField(max_length=32, null=False, blank=True)

    price = models.FloatField(blank=True, null=False)
    currency = models.IntegerField(null=False, default=2, choices=CommonChoices.CURRENCY_CHOICES)

    description = models.TextField(max_length=256)
    invoice_url = models.CharField(max_length=32, null = True)

    invoice_date = models.DateTimeField(default=now, blank=True, null=True)

    insert_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="insert_invoice", blank=False)
    insert_date = models.DateTimeField(default=now, editable=False)

    product = models.ForeignKey(Product, on_delete=models.CASCADE, blank=False)

    quantity = models.FloatField(blank=True, null=False)
    quantity_type = models.IntegerField(null=False, choices=CommonChoices.QUANTITY_TYPE_CHOICES)

    unit_price = models.FloatField(blank=True, null=True)

    e_invoice = models.BooleanField(default=True)

    @staticmethod
    def get_invoice(**kwargs):
        """
        @brief    If the invoice with the given arguments exists, returns the invoice. Otherwise returns None
        """

        invoice = None
        try:
            invoice = Invoice.objects.get(**kwargs)
        except Invoice.DoesNotExist:
            pass

        return invoice