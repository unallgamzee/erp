from rest_framework import serializers

# Daily Price
from api.daily_price.models import DailyPrice

# General
from api.field_names import DailyPriceFields

# Serializers
from api.product.serializers import ProductSummarySerializer
from api.user.serializers import UserSummarySerializer


class DailyPriceCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = DailyPrice
        fields = (
            DailyPriceFields.product,
            DailyPriceFields.price,
            DailyPriceFields.currency,
            DailyPriceFields.daily_date,

            DailyPriceFields.insert_user
        )


class DailyPriceSummarySerializer(serializers.ModelSerializer):

    product = ProductSummarySerializer(many=False, read_only=True)
    insert_user = UserSummarySerializer(many=False, read_only=True)

    class Meta:
        model = DailyPrice
        fields = (
            DailyPriceFields.daily_price_id,
            DailyPriceFields.daily_date,
            
            DailyPriceFields.product,
            DailyPriceFields.price,
            DailyPriceFields.currency,

            DailyPriceFields.insert_user,
            DailyPriceFields.insert_date
        )


class DailyPriceUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = DailyPrice
        fields = (
            DailyPriceFields.product,
            DailyPriceFields.price,
            DailyPriceFields.currency
        )
