# Django
from django.db import models

from api.user.models import User
from api.product.models import Product

from api.field_names import CommonChoices

from django.utils.timezone import now


class DailyPrice(models.Model):
    product = models.ForeignKey(Product, on_delete=models.CASCADE, blank=False)
    daily_date = models.DateTimeField(default=now, editable=False)

    price = models.FloatField(default=0.0, null=False, blank=False)
    currency = models.IntegerField(null=False, default=2, choices=CommonChoices.CURRENCY_CHOICES)

    insert_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="insert_daily_price")
    insert_date = models.DateTimeField(default=now, editable=False)

    @staticmethod
    def get_daily_price(**kwargs):
        """
        @brief    If the price with the given arguments exists, returns the price. Otherwise returns None
        """

        daily_price = None
        try:
            daily_price = DailyPrice.objects.get(**kwargs)
        except DailyPrice.DoesNotExist:
            pass

        return daily_price
