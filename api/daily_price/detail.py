# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Daily Price
from api.daily_price.models import DailyPrice

# Serializers
from api.daily_price.serializers import DailyPriceUpdateSerializer, DailyPriceSummarySerializer

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC
from api.field_names import DailyPriceFields
import datetime

from rest_framework.permissions import IsAuthenticated


class DailyPriceDetailView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, price_id, format=None):
        daily_price = DailyPrice.get_daily_price(id=price_id)
        if daily_price is not None:
            serializer = DailyPriceSummarySerializer(daily_price, many=False, read_only=True)
            response_data = {
                "daily-price": serializer.data
            }

            response = Response(response_data, status=status.HTTP_200_OK)

        else:
            errors = {
                "daily-price_id": [
                    _("Daily price ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A daily price with the given ID doesn't exist."),
                                             SC.daily_price_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def delete(self, request, price_id, format=None):
        daily_price = DailyPrice.get_daily_price(id=price_id)
        if daily_price is not None:
            serializer = DailyPriceSummarySerializer(daily_price, many=False, read_only=True)
            response_data = {
                "daily-price": serializer.data
            }

            daily_price.delete()
            response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "daily-price-id": [
                    _("Daily price ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A daily price with the given ID doesn't exist."),
                                             SC.daily_price_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response
