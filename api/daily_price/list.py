# Django
from django.core.paginator import Paginator

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Daily Price
from api.daily_price.serializers import DailyPriceSummarySerializer
from api.daily_price.models import DailyPrice

# General
from api.field_names import DailyPriceFields

from rest_framework.permissions import IsAuthenticated


class DailyPriceListView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):

        daily_prices = DailyPrice.objects.all()

        serializer = DailyPriceSummarySerializer(daily_prices, many=True, read_only=True)
        response_data = {
            "daily-prices": serializer.data
        }

        return Response(response_data, status=status.HTTP_200_OK)