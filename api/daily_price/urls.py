# Django
from django.conf.urls import url

# Daily Price
from api.daily_price.new import DailyPriceCreateView
from api.daily_price.detail import DailyPriceDetailView
from api.daily_price.list import DailyPriceListView

urlpatterns = [
    url(r'^daily-prices/new/$', DailyPriceCreateView.as_view(), name="price-new"),
    url(r'^daily-prices/$', DailyPriceListView.as_view(), name="price-list"),
    url(r'^daily-prices/(?P<price_id>[0-9]+)/$', DailyPriceDetailView.as_view(), name="price-detail")
]
