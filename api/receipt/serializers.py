from rest_framework import serializers

# User
from api.receipt.models import Receipt

# General
from api.field_names import ReceiptFields

# Serializers
from api.company.serializers import CompanySummarySerializer
from api.contact.serializers import ContactSummarySerializer
from api.product.serializers import ProductSummarySerializer
from api.user.serializers import UserSummarySerializer


class ReceiptCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Receipt
        fields = (
            ReceiptFields.receipt_no,
            ReceiptFields.product,
            ReceiptFields.status,

            ReceiptFields.quantity,
            ReceiptFields.quantity_type,

            ReceiptFields.price,
            ReceiptFields.currency,

            ReceiptFields.contact,
            ReceiptFields.company,

            ReceiptFields.description,
            ReceiptFields.receipt_url,

            ReceiptFields.receipt_date,

            ReceiptFields.insert_user,
            ReceiptFields.insert_date,

            ReceiptFields.store,
            ReceiptFields.receipt_type
        )


class ReceiptSummarySerializer(serializers.ModelSerializer):

    company =  CompanySummarySerializer(many=False, read_only=True)
    contact = ContactSummarySerializer(many=False, read_only=True)
    product = ProductSummarySerializer(many=False, read_only=True)
    insert_user = UserSummarySerializer(many=False, read_only=True)

    class Meta:
        model = Receipt
        fields = (
            ReceiptFields.receipt_id,
            ReceiptFields.receipt_no,

            ReceiptFields.status,
            ReceiptFields.product,

            ReceiptFields.quantity,
            ReceiptFields.quantity_type,

            ReceiptFields.price,
            ReceiptFields.currency,

            ReceiptFields.contact,
            ReceiptFields.company,

            ReceiptFields.description,
            ReceiptFields.receipt_url,

            ReceiptFields.receipt_date,

            ReceiptFields.insert_user,
            ReceiptFields.insert_date,
            ReceiptFields.store,
            ReceiptFields.receipt_type
        )


class ReceiptUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Receipt
        fields = (
            ReceiptFields.status,
            ReceiptFields.description
        )