# Django
from django.core.paginator import Paginator

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Receipt
from api.receipt.serializers import ReceiptSummarySerializer
from api.receipt.models import Receipt

# General
from api.field_names import ReceiptFields

from rest_framework.permissions import IsAuthenticated


class ReceiptListView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):

        receipts = Receipt.objects.all()

        serializer = ReceiptSummarySerializer(receipts, many=True, read_only=True)
        response_data = {
            "receipts": serializer.data
        }

        return Response(response_data, status=status.HTTP_200_OK)