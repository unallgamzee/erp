# Django
from django.conf.urls import url

# Receipt
from api.receipt.new import ReceiptCreateView
from api.receipt.detail import ReceiptDetailView
from api.receipt.list import ReceiptListView

urlpatterns = [
    url(r'^receipts/new/$', ReceiptCreateView.as_view(), name="receipt-new"),
    url(r'^receipts/$', ReceiptListView.as_view(), name="receipt-list"),
    url(r'^receipts/(?P<receipt_id>[0-9]+)/$', ReceiptDetailView.as_view(), name="receipt-detail")
]
