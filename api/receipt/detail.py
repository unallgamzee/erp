# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Receipt
from api.receipt.models import Receipt

# Serializers
from api.receipt.serializers import ReceiptSummarySerializer, ReceiptUpdateSerializer

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC
from api.field_names import ReceiptFields
import datetime

from rest_framework.permissions import IsAuthenticated


class ReceiptDetailView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, receipt_id, format=None):
        receipt = Receipt.get_receipt(id=receipt_id, status=1)
        if receipt is not None:
            serializer = ReceiptSummarySerializer(receipt, many=False, read_only=True)
            response_data = {
                "receipt": serializer.data
            }

            response = Response(response_data, status=status.HTTP_200_OK)

        else:
            errors = {
                "receipt-id": [
                    _("Receipt ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A receipt with the given ID doesn't exist."), SC.receipt_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def delete(self, request, receipt_id, format=None):
        receipt = Receipt.get_receipt(id=receipt_id, status=1)
        if receipt is not None:
            receipt.status = 0
            receipt.save()

            serializer = ReceiptSummarySerializer(receipt, many=False, read_only=True)
            response_data = {
                "receipt": serializer.data
            }
            response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "receipt-id": [
                    _("Receipt ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A receipt with the given ID doesn't exist."), SC.receipt_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def put(self, request, receipt_id, format=None):
        saved_receipt = None
        payload = request.data

        receipt = Receipt.get_receipt(id=receipt_id, status=1)
        if receipt is not None:
            receipt_data = self.extract_receipt_data(payload, receipt)
            if len(receipt_data) == 0:
                response = Response(status=status.HTTP_304_NOT_MODIFIED)
            else:
                errors = {}
                serializer = ReceiptUpdateSerializer(receipt, data=receipt_data, partial=True, many=False)
                if serializer.is_valid():
                    saved_receipt = serializer.save()
                    saved_receipt.save()
                else:
                    errors.update(serializer.errors)

                if len(errors) > 0:
                    generated_status = SG.get_status(_("Error occurred while updating the receipt."),
                                                     SC.parameter_error,
                                                     errors)
                    response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)
                else:
                    detail_serializer = ReceiptSummarySerializer(saved_receipt, read_only=True, many=False)
                    response_data = {
                        "receipt": detail_serializer.data
                    }
                    response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "receipt-id": [
                    _("Receipt ID doesn't exist.")
                ]
            }
            generated_status = SG.get_status(_("A receipt with the given ID doesn't exist."), SC.receipt_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def extract_receipt_data(self, payload, receipt):
        receipt_data = {}

        if ReceiptFields.status in payload:
            temp_value = payload.pop(ReceiptFields.status)
            if receipt.status != temp_value:
                receipt_data[ReceiptFields.status] = temp_value

        if ReceiptFields.description in payload:
            temp_value = payload.pop(ReceiptFields.description)
            if receipt.description != temp_value:
                receipt_data[ReceiptFields.description] = temp_value

        return receipt_data
