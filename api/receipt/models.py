# Django
from django.db import models

from api.user.models import User
from api.contact.models import Contact

from api.company.models import Company
from api.product.models import Product

from django.utils.timezone import now

from api.field_names import CommonChoices


class Receipt(models.Model):
    receipt_no = models.CharField(max_length=32)
    receipt_type = models.IntegerField(null=False, choices=CommonChoices.FINANCE_TYPE_CHOICES)

    product = models.ForeignKey(Product, on_delete=models.CASCADE, blank=False)
    status = models.IntegerField(null=False, choices=CommonChoices.FINANCE_STATUS_CHOICES)

    contact = models.ForeignKey(Contact, on_delete=models.CASCADE, blank=False)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, null=True)

    quantity = models.FloatField(blank=True, null=True)
    quantity_type = models.IntegerField(null=True, choices=CommonChoices.QUANTITY_TYPE_CHOICES)

    price = models.FloatField(default=0.0, null=False, blank=False)
    currency = models.IntegerField(null=False, default=2, choices=CommonChoices.CURRENCY_CHOICES)

    description = models.TextField(max_length=256)
    receipt_url = models.CharField(max_length=32, null=True)

    receipt_date = models.DateTimeField(default=now, blank=True, null=True)

    insert_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="insert_receipt", blank=False)
    insert_date = models.DateTimeField(default=now, editable=False)

    store = models.ForeignKey('api.Store', on_delete=models.CASCADE, null=True)

    @staticmethod
    def get_receipt(**kwargs):
        """
        @brief    If the receipt with the given arguments exists, returns the receipt. Otherwise returns None
        """

        receipt = None
        try:
            receipt = Receipt.objects.get(**kwargs)
        except Receipt.DoesNotExist:
            pass

        return receipt