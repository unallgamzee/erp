# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Company
from api.company.models import Company

from rest_framework.permissions import IsAuthenticated

# Serializers
from api.company.serializers import CompanySummarySerializer, CompanyUpdateSerializer

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC
from api.field_names import CompanyFields
import datetime


class CompanyDetailView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, company_id, format=None):
        company = Company.get_company(id=company_id, is_active=True)
        if company is not None:
            serializer = CompanySummarySerializer(company, many=False, read_only=True)
            response_data = {
                "company": serializer.data
            }

            response = Response(response_data, status=status.HTTP_200_OK)

        else:
            errors = {
                "company-id": [
                    _("Company ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A company with the given ID doesn't exist."), SC.company_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response


    def delete(self, request, company_id, format=None):
        company = Company.get_company(id=company_id, is_active=True)
        if company is not None:
            serializer = CompanySummarySerializer(company, many=False, read_only=True)
            response_data = {
                "company": serializer.data
            }

            company.is_active = False
            company.save()
            response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "company-id": [
                    _("Company ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A company with the given ID doesn't exist."), SC.company_not_exist, errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response


    def put(self, request, company_id, format=None):
        saved_company = None
        payload = request.data

        company = Company.get_company(id=company_id, is_active=True)
        if company is not None:
            company_data = self.extract_company_data(payload, company)
            if len(company_data) == 0:
                response = Response(status=status.HTTP_304_NOT_MODIFIED)
            else:
                errors = {}
                company_serializer = CompanyUpdateSerializer(company, data=company_data, partial=True, many=False)
                if company_serializer.is_valid():
                    saved_company = company_serializer.save()
                    saved_company.save()
                else:
                    errors.update(company_serializer.errors)

                if len(errors) > 0:
                    generated_status = SG.get_status(_("Error occurred while updating the company."), SC.parameter_error,
                                                     errors)
                    response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)
                else:
                    detail_serializer = CompanySummarySerializer(saved_company, read_only=True, many=False)
                    response_data = {
                        "company": detail_serializer.data
                    }
                    response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "company-id": [
                    _("Company ID doesn't exist.")
                ]
            }
            generated_status = SG.get_status(_("A company with the given ID doesn't exist."), SC.company_not_exist, errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response


    def extract_company_data(self, payload, company):
        company_data = {}
        if CompanyFields.name in payload:
            temp_value = payload.pop(CompanyFields.name)
            if company.name != temp_value:
                company_data[CompanyFields.name] = temp_value

        if CompanyFields.phone in payload:
            temp_value = payload.pop(CompanyFields.phone)
            if company.phone != temp_value:
                company_data[CompanyFields.phone] = temp_value

        if CompanyFields.fax in payload:
            temp_value = payload.pop(CompanyFields.fax)
            if company.fax != temp_value:
                company_data[CompanyFields.fax] = temp_value

        if CompanyFields.email in payload:
            temp_value = payload.pop(CompanyFields.email)
            if company.email != temp_value:
                company_data[CompanyFields.email] = temp_value

        if CompanyFields.tax_office in payload:
            temp_value = payload.pop(CompanyFields.tax_office)
            if company.tax_office != temp_value:
                company_data[CompanyFields.tax_office] = temp_value

        if CompanyFields.tax_no in payload:
            temp_value = payload.pop(CompanyFields.tax_no)
            if company.tax_no != temp_value:
                company_data[CompanyFields.tax_no] = temp_value

        if CompanyFields.banking_accounts in payload:
            temp_value = payload.pop(CompanyFields.banking_accounts)
            if company.banking_accounts != temp_value:
                company_data[CompanyFields.banking_accounts] = temp_value

        if CompanyFields.description in payload:
            temp_value = payload.pop(CompanyFields.description)
            if company.description != temp_value:
                company_data[CompanyFields.description] = temp_value

        if CompanyFields.country in payload:
            temp_value = payload.pop(CompanyFields.country)
            if company.country != temp_value:
                company_data[CompanyFields.country] = temp_value

        if CompanyFields.city in payload:
            temp_value = payload.pop(CompanyFields.city)
            if company.city != temp_value:
                company_data[CompanyFields.city] = temp_value

        return company_data
