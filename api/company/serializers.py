from rest_framework import serializers

# Company
from api.company.models import Company

# General
from api.field_names import CompanyFields

# Serializers
from api.country.serializers import CountrySerializer, CitySerializer
from api.user.serializers import UserSummarySerializer


class CompanyCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = (
            CompanyFields.name,
            CompanyFields.phone,
            CompanyFields.fax,
            CompanyFields.email,
            CompanyFields.tax_office,
            CompanyFields.tax_no,
            CompanyFields.banking_accounts,
            CompanyFields.description,
            CompanyFields.country,
            CompanyFields.city,
            CompanyFields.insert_user
        )


class CompanySummarySerializer(serializers.ModelSerializer):

    country = CountrySerializer(many=False, read_only=True)
    city = CitySerializer(many=False, read_only=True)
    insert_user = UserSummarySerializer(many=False, read_only=True)

    class Meta:
        model = Company
        fields = (
            CompanyFields.company_id,
            CompanyFields.name,
            CompanyFields.phone,
            CompanyFields.fax,
            CompanyFields.email,
            CompanyFields.tax_office,
            CompanyFields.tax_no,
            CompanyFields.banking_accounts,
            CompanyFields.description,
            CompanyFields.is_active,
            CompanyFields.country,
            CompanyFields.city,
            CompanyFields.insert_user,
            CompanyFields.insert_date
        )


class CompanyUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Company
        fields = (
            CompanyFields.name,
            CompanyFields.phone,
            CompanyFields.fax,
            CompanyFields.email,
            CompanyFields.tax_office,
            CompanyFields.tax_no,
            CompanyFields.banking_accounts,
            CompanyFields.description,
            CompanyFields.is_active,
            CompanyFields.country,
            CompanyFields.city,
            CompanyFields.insert_user,
            CompanyFields.insert_date,
        )
