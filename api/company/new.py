# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC

# company
from api.company.serializers import CompanyCreateSerializer, CompanySummarySerializer

from rest_framework.permissions import IsAuthenticated


class CompanyCreateView(APIView):

    permission_classes = (IsAuthenticated, )

    def post(self, request, format=None):
        payload = request.data
        payload['insert_user'] = request.user.id
        serializer = CompanyCreateSerializer(data=payload, many=False)

        if serializer.is_valid():
            company = serializer.save()
            detail_serializer = CompanySummarySerializer(company, read_only=True, many=False)
            response_data = {
                'company': detail_serializer.data
            }
            response = Response(response_data, status=status.HTTP_201_CREATED)
        else:
            generated_status = SG.get_status(_("Parameter error."), SC.parameter_error, serializer.errors)
            response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)

        return response
