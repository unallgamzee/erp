# Django
from django.core.paginator import Paginator

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Company
from api.company.serializers import CompanySummarySerializer
from api.company.models import Company

# General
from api.field_names import CompanyFields

from rest_framework.permissions import IsAuthenticated


class CompanyListView(APIView):
    
    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):

        companies = Company.objects.all()

        serializer = CompanySummarySerializer(companies, many=True, read_only=True)
        response_data = {
            "companies": serializer.data
        }

        return Response(response_data, status=status.HTTP_200_OK)