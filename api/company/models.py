# Django
from django.db import models

from django.core.validators import RegexValidator
from django.utils.translation import ugettext as _

from api.user.models import User

from api.country.models import Country, City

from django.utils.timezone import now


class Company(models.Model):
    name = models.CharField(max_length=32, null=False, blank=True)

    phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message=_(
        "Phone number must be entered in the format: '+999999999'. Up to 15 digits allowed."))
    phone = models.CharField(max_length=15, validators=[phone_regex], blank=True)

    fax_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$', message=_(
        "Fax number must be entered in the format: '+999999999'. Up to 15 digits allowed."))
    fax = models.CharField(max_length=15, validators=[fax_regex], blank=True)

    email = models.EmailField(blank=False, unique=True)

    tax_office = models.CharField(max_length=32, null=False, blank=True, default=None)
    tax_no = models.CharField(max_length=32, null=False, blank=True, default=None)

    banking_accounts = models.CharField(max_length=32, null=False, blank=True, default=None)

    description = models.TextField(max_length=256)
    is_active = models.BooleanField(default=True)

    country = models.ForeignKey(Country, on_delete=models.SET_NULL, null=True, blank=True)
    city = models.ForeignKey(City, on_delete=models.SET_NULL, null=True, blank=True)

    insert_user = models.ForeignKey(User, on_delete=models.CASCADE, blank=False)
    insert_date = models.DateTimeField(default=now, editable=False)
    @staticmethod
    def get_company(**kwargs):
        """
        @brief    If the company with the given arguments exists, returns the company. Otherwise returns None
        """

        company = None
        try:
            company = Company.objects.get(**kwargs)
        except Company.DoesNotExist:
            pass

        return company
