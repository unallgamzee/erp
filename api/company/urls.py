# Django
from django.conf.urls import url

# Session
from api.company.new import CompanyCreateView
from api.company.detail import CompanyDetailView
from api.company.list import CompanyListView

urlpatterns = [
    url(r'^companies/new/$', CompanyCreateView.as_view(), name="company-new"),
    url(r'^companies/$', CompanyListView.as_view(), name="company-list"),
    url(r'^companies/(?P<company_id>[0-9]+)/$', CompanyDetailView.as_view(), name="company-detail")
]
