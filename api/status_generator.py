class StatusCodes:
    """
    @brief    These codes are used in the status message. **DO NOT** use these as the HTTP response code.
    """

    unknown = -1

    unsupported_version = 10

    parameter_error = 11

    insufficient_permissions = 12

    already_exists = 13

    missing_token = 14

    invalid_token = 15

    waybill_not_exist = 16

    invoice_not_exist = 17

    receipt_not_exist = 18

    company_not_exist = 19

    contact_not_exist = 20

    product_not_exist = 21

    unit_type_not_exist = 22

    user_not_exist = 23

    feature_not_exist = 24

    session_not_exist = 25

    store_not_exist = 26

    product_price_not_exist = 27

    product_type_not_exist = 28

    cash_flow_not_exist = 29

    sub_product_not_exist = 30

    daily_price_not_exist = 31

    waybill_product_not_exist = 32

    invoice_product_not_exist = 33

    receipt_product_not_exist = 34
    
    expensive_not_exist = 35


class StatusGenerator:
    @staticmethod
    def get_status(message, code, errors):
        status = {
            'status': {
                'message': message,
                'code': code,
                'parameters': errors
            }
        }

        if len(errors) > 0:
            status['status']['parameters'] = errors

        return status
