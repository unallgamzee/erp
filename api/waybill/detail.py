# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Waybill
from api.waybill.models import Waybill

# Serializers
from api.waybill.serializers import WaybillSummarySerializer, WaybillUpdateSerializer

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC
from api.field_names import WaybillFields
import datetime

from rest_framework.permissions import IsAuthenticated


class WaybillDetailView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, waybill_id, format=None):
        waybill = Waybill.get_waybill(id=waybill_id)
        if waybill is not None:
            serializer = WaybillSummarySerializer(waybill, many=False, read_only=True)
            response_data = {
                "waybill": serializer.data
            }

            response = Response(response_data, status=status.HTTP_200_OK)

        else:
            errors = {
                "waybill-id": [
                    _("Waybill ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A waybill with the given ID doesn't exist."), SC.waybill_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def delete(self, request, waybill_id, format=None):
        waybill = Waybill.get_waybill(id=waybill_id, status=1)
        if waybill is not None:
            waybill.status = 0
            waybill.save()

            serializer = WaybillSummarySerializer(waybill, many=False, read_only=True)
            response_data = {
                "waybill": serializer.data
            }
            response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "waybill-id": [
                    _("Waybill ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A waybill with the given ID doesn't exist."), SC.waybill_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def put(self, request, waybill_id, format=None):
        saved_waybill = None
        payload = request.data

        waybill = Waybill.get_waybill(id=waybill_id, status=1)
        if waybill is not None:
            waybill_data = self.extract_waybill_data(payload, waybill)
            if len(waybill_data) == 0:
                response = Response(status=status.HTTP_304_NOT_MODIFIED)
            else:
                errors = {}
                serializer = WaybillUpdateSerializer(waybill, data=waybill_data, partial=True, many=False)
                if serializer.is_valid():
                    saved_waybill = serializer.save()
                    saved_waybill.save()
                else:
                    errors.update(serializer.errors)

                if len(errors) > 0:
                    generated_status = SG.get_status(_("Error occurred while updating the waybill."),
                                                     SC.parameter_error,
                                                     errors)
                    response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)
                else:
                    detail_serializer = WaybillSummarySerializer(saved_waybill, read_only=True, many=False)
                    response_data = {
                        "waybill": detail_serializer.data
                    }
                    response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "waybill-id": [
                    _("Waybill ID doesn't exist.")
                ]
            }
            generated_status = SG.get_status(_("A waybill with the given ID doesn't exist."), SC.waybill_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def extract_waybill_data(self, payload, waybill):
        waybill_data = {}

        if WaybillFields.waybill_type in payload:
            temp_value = payload.pop(WaybillFields.waybill_type)
            if waybill.waybill_type != temp_value:
                waybill_data[WaybillFields.waybill_type] = temp_value

        if WaybillFields.status in payload:
            temp_value = payload.pop(WaybillFields.status)
            if waybill.status != temp_value:
                waybill_data[WaybillFields.status] = temp_value

        if WaybillFields.contact in payload:
            temp_value = payload.pop(WaybillFields.contact)
            if waybill.contact != temp_value:
                waybill_data[WaybillFields.contact] = temp_value

        if WaybillFields.company in payload:
            temp_value = payload.pop(WaybillFields.company)
            if waybill.company != temp_value:
                waybill_data[WaybillFields.company] = temp_value

        if WaybillFields.serial_no in payload:
            temp_value = payload.pop(WaybillFields.serial_no)
            if waybill.serial_no != temp_value:
                waybill_data[WaybillFields.serial_no] = temp_value

        if WaybillFields.order_no in payload:
            temp_value = payload.pop(WaybillFields.order_no)
            if waybill.order_no != temp_value:
                waybill_data[WaybillFields.order_no] = temp_value

        if WaybillFields.description in payload:
            temp_value = payload.pop(WaybillFields.description)
            if waybill.description != temp_value:
                waybill_data[WaybillFields.description] = temp_value

        if WaybillFields.waybill_url in payload:
            temp_value = payload.pop(WaybillFields.waybill_url)
            if waybill.waybill_url != temp_value:
                waybill_data[WaybillFields.waybill_url] = temp_value

        if WaybillFields.dispatch_date in payload:
            temp_value = payload.pop(WaybillFields.dispatch_date)
            if waybill.dispatch_date != temp_value:
                waybill_data[WaybillFields.dispatch_date] = temp_value

        if WaybillFields.delivery_date in payload:
            temp_value = payload.pop(WaybillFields.delivery_date)
            if waybill.delivery_date != temp_value:
                waybill_data[WaybillFields.delivery_date] = temp_value

        if WaybillFields.product in payload:
            temp_value = payload.pop(WaybillFields.product)
            if waybill.product != temp_value:
                waybill_data[WaybillFields.product] = temp_value

        if WaybillFields.quantity in payload:
            temp_value = payload.pop(WaybillFields.quantity)
            if waybill.quantity != temp_value:
                waybill_data[WaybillFields.quantity] = temp_value

        if WaybillFields.quantity_type in payload:
            temp_value = payload.pop(WaybillFields.quantity_type)
            if waybill.quantity_type != temp_value:
                waybill_data[WaybillFields.quantity_type] = temp_value

        return waybill_data