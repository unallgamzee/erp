# Django
from django.conf.urls import url

# Waybill
from api.waybill.new import WaybillCreateView
from api.waybill.detail import WaybillDetailView
from api.waybill.list import WaybillListView

urlpatterns = [
    url(r'^waybills/new/$', WaybillCreateView.as_view(), name="waybill-new"),
    url(r'^waybills/$', WaybillListView.as_view(), name="waybill-list"),
    url(r'^waybills/(?P<waybill_id>[0-9]+)/$', WaybillDetailView.as_view(), name="waybill-detail")
]
