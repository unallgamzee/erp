# Django
from django.db import models

from api.user.models import User
from api.company.models import Company
from api.contact.models import Contact

from api.product.models import Product
from django.utils.timezone import now

from api.field_names import CommonChoices


class Waybill(models.Model):
    waybill_type = models.IntegerField(null=False, choices=CommonChoices.FINANCE_TYPE_CHOICES)
    waybill_no = models.CharField(max_length=32, unique=True, null=False, blank=True)

    status = models.IntegerField(default=1, null=False, choices=CommonChoices.FINANCE_STATUS_CHOICES)

    contact = models.ForeignKey(Contact, on_delete=models.CASCADE, blank=False)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, null=True)

    serial_no = models.CharField(max_length=32, null=False, blank=True)
    order_no = models.CharField(max_length=32, null=False, blank=True)

    description = models.TextField(max_length=256)
    waybill_url = models.CharField(max_length=32, null=True)

    dispatch_date = models.DateTimeField(blank=True, null=True)
    delivery_date = models.DateTimeField(blank=True, null=True)

    insert_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="insert_waybill", blank=False)
    insert_date = models.DateTimeField(default=now, editable=False)

    product = models.ForeignKey(Product, on_delete=models.CASCADE, blank=False)

    quantity = models.FloatField(blank=True, null=False)
    quantity_type = models.IntegerField(null=False, choices=CommonChoices.QUANTITY_TYPE_CHOICES)
    store = models.ForeignKey('api.Store', on_delete=models.CASCADE)

    is_invoice = models.BooleanField(default=False)


    @staticmethod
    def get_waybill(**kwargs):
        """
        @brief    If the waybill with the given arguments exists, returns the waybill. Otherwise returns None
        """

        waybill = None
        try:
            waybill = Waybill.objects.get(**kwargs)
        except Waybill.DoesNotExist:
            pass

        return waybill

