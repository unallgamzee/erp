# Django
from django.core.paginator import Paginator

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Waybill
from api.waybill.serializers import WaybillSummarySerializer
from api.waybill.models import Waybill

# General
from api.field_names import WaybillFields

from rest_framework.permissions import IsAuthenticated


class WaybillListView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):
        waybills = Waybill.objects.all()

        serializer = WaybillSummarySerializer(waybills, many=True, read_only=True)
        response_data = {
            "waybills": serializer.data
        }

        return Response(response_data, status=status.HTTP_200_OK)