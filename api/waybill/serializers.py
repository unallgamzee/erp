from rest_framework import serializers

# Waybill and Product
from api.waybill.models import Waybill

# General
from api.field_names import WaybillFields

# Serializers
from api.company.serializers import CompanySummarySerializer
from api.contact.serializers import ContactSummarySerializer
from api.product.serializers import ProductSummarySerializer
from api.user.serializers import UserSummarySerializer


class WaybillCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Waybill
        fields = (
            WaybillFields.waybill_type,
            WaybillFields.waybill_no,
            WaybillFields.status,
            WaybillFields.contact,
            WaybillFields.company,
            WaybillFields.serial_no,
            WaybillFields.order_no,
            WaybillFields.description,
            WaybillFields.waybill_url,
            WaybillFields.dispatch_date,
            WaybillFields.delivery_date,
            WaybillFields.insert_user,
            WaybillFields.insert_date,
            WaybillFields.product,
            WaybillFields.quantity,
            WaybillFields.quantity_type,
            WaybillFields.store
        )


class WaybillSummarySerializer(serializers.ModelSerializer):

    company =  CompanySummarySerializer(many=False, read_only=True)
    contact = ContactSummarySerializer(many=False, read_only=True)
    product = ProductSummarySerializer(many=False, read_only=True)
    insert_user = UserSummarySerializer(many=False, read_only=True)

    class Meta:
        model = Waybill
        fields = (
            WaybillFields.waybill_id,
            WaybillFields.waybill_no,
            WaybillFields.waybill_type,
            WaybillFields.status,
            WaybillFields.contact,
            WaybillFields.company,
            WaybillFields.serial_no,
            WaybillFields.order_no,
            WaybillFields.description,
            WaybillFields.waybill_url,
            WaybillFields.dispatch_date,
            WaybillFields.delivery_date,
            WaybillFields.insert_user,
            WaybillFields.insert_date,
            WaybillFields.product,
            WaybillFields.quantity,
            WaybillFields.quantity_type,
            WaybillFields.store,
            WaybillFields.is_invoice
        )


class WaybillUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Waybill
        fields = (
            WaybillFields.waybill_type,
            WaybillFields.status,
            WaybillFields.contact,
            WaybillFields.company,
            WaybillFields.serial_no,
            WaybillFields.order_no,
            WaybillFields.description,
            WaybillFields.waybill_url,
            WaybillFields.dispatch_date,
            WaybillFields.delivery_date,
            WaybillFields.product,
            WaybillFields.quantity,
            WaybillFields.quantity_type
        )

