from rest_framework import serializers

# General Feature
from api.general_feature.models import GeneralFeature

# General
from api.field_names import GeneralFeatureFields

# Serializers
from api.company.serializers import CompanySummarySerializer
from api.user.serializers import UserSummarySerializer


class GeneralFeatureCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = GeneralFeature
        fields = (
            GeneralFeatureFields.company,
            GeneralFeatureFields.currency,
            GeneralFeatureFields.insert_user,
            GeneralFeatureFields.unit_type
        )


class GeneralFeatureSummarySerializer(serializers.ModelSerializer):

    company = CompanySummarySerializer(many=False, read_only=True)
    insert_user = UserSummarySerializer(many=False, read_only=True)

    class Meta:
        model = GeneralFeature
        fields = (
            GeneralFeatureFields.feature_id,
            GeneralFeatureFields.company,
            GeneralFeatureFields.currency,
            GeneralFeatureFields.unit_type,
            GeneralFeatureFields.insert_user,
            GeneralFeatureFields.insert_date
        )


class GeneralFeatureUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = GeneralFeature
        fields = (
            GeneralFeatureFields.company,
            GeneralFeatureFields.currency,
            GeneralFeatureFields.unit_type
        )
