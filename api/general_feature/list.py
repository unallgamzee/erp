# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# General Feature
from api.general_feature.models import GeneralFeature
from api.general_feature.serializers import GeneralFeatureSummarySerializer

from rest_framework.permissions import IsAuthenticated


class GeneralFeatureListView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):
        features = GeneralFeature.objects.all()
        serializer = GeneralFeatureSummarySerializer(features, many=True, read_only=True)
        payload = {
            'general-features': serializer.data
        }

        return Response(payload, status=status.HTTP_200_OK)
