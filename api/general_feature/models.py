# Django
from django.db import models

from api.user.models import User
from api.company.models import Company

from api.field_names import CommonChoices

from django.utils.timezone import now


class GeneralFeature(models.Model):

    company = models.OneToOneField(Company, on_delete=models.CASCADE, blank=False)
    currency = models.IntegerField(null=False, default=2, choices=CommonChoices.CURRENCY_CHOICES)

    unit_type = models.IntegerField(null=False, choices=CommonChoices.QUANTITY_TYPE_CHOICES)

    insert_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="insert_feature", blank=False)
    insert_date = models.DateTimeField(default=now, editable=False)

    @staticmethod
    def get_general_feature(**kwargs):
        """
        @brief    If the feature with the given arguments exists, returns the feature. Otherwise returns None
        """

        feature = None
        try:
            feature = GeneralFeature.objects.get(**kwargs)
        except GeneralFeature.DoesNotExist:
            pass

        return feature
