# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# General Feature
from api.general_feature.models import GeneralFeature

# Serializers
from api.general_feature.serializers import GeneralFeatureUpdateSerializer, GeneralFeatureSummarySerializer

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC
from api.field_names import GeneralFeatureFields
import datetime

from rest_framework.permissions import IsAuthenticated


class GeneralFeatureDetailView(APIView):

    permission_classes = (IsAuthenticated, )

    def put(self, request, feature_id, format=None):
        saved_feature = None
        payload = request.data

        general_feature = GeneralFeature.get_general_feature(id=feature_id)
        if general_feature is not None:
            feature_data = self.extract_feature_data(payload, general_feature)
            if len(feature_data) == 0:
                response = Response(status=status.HTTP_304_NOT_MODIFIED)
            else:
                errors = {}
                feature_serializer = GeneralFeatureUpdateSerializer(general_feature, data=feature_data, partial=True,
                                                                    many=False)
                if feature_serializer.is_valid():
                    saved_feature = feature_serializer.save()
                    saved_feature.save()
                else:
                    errors.update(feature_serializer.errors)

                if len(errors) > 0:
                    generated_status = SG.get_status(_("Error occurred while updating the general feature."),
                                                     SC.parameter_error,
                                                     errors)
                    response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)
                else:
                    detail_serializer = GeneralFeatureSummarySerializer(saved_feature, read_only=True, many=False)
                    response_data = {
                        "general-feature": detail_serializer.data
                    }
                    response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "general-feature-id": [
                    _("General Feature ID doesn't exist.")
                ]
            }
            generated_status = SG.get_status(_("A general feature with the given ID doesn't exist."),
                                             SC.feature_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def extract_feature_data(self, payload, general_feature):
        feature_data = {}

        if GeneralFeatureFields.company in payload:
            temp_value = payload.pop(GeneralFeatureFields.company)
            if general_feature.company != temp_value:
                feature_data[GeneralFeatureFields.company] = temp_value

        if GeneralFeatureFields.currency in payload:
            temp_value = payload.pop(GeneralFeatureFields.currency)
            if general_feature.currency != temp_value:
                feature_data[GeneralFeatureFields.currency] = temp_value

        if GeneralFeatureFields.unit_type in payload:
            temp_value = payload.pop(GeneralFeatureFields.unit_type)
            if general_feature.unit_type != temp_value:
                feature_data[GeneralFeatureFields.unit_type] = temp_value

        return feature_data
