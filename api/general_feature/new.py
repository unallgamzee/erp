# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC

# General Feature
from api.general_feature.serializers import GeneralFeatureCreateSerializer, GeneralFeatureSummarySerializer

from rest_framework.permissions import IsAuthenticated


class GeneralFeatureCreateView(APIView):

    permission_classes = (IsAuthenticated, )

    def post(self, request, format=None):
        payload = request.data
        serializer = GeneralFeatureCreateSerializer(data=payload, many=False)

        if serializer.is_valid():
            general_feature = serializer.save()
            detail_serializer = GeneralFeatureSummarySerializer(general_feature, read_only=True, many=False)
            response_data = {
                'general_feature': detail_serializer.data
            }
            response = Response(response_data, status=status.HTTP_201_CREATED)
        else:
            generated_status = SG.get_status(_("Parameter error."), SC.parameter_error, serializer.errors)
            response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)

        return response
