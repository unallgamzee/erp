# Django
from django.conf.urls import url

# GeneralFeature
from api.general_feature.list import GeneralFeatureListView
from api.general_feature.new import GeneralFeatureCreateView
from api.general_feature.detail import GeneralFeatureDetailView

urlpatterns = [
    # Country
    url(r'^general-features/$', GeneralFeatureListView.as_view(), name="feature-list"),
    url(r'^general-features/new/$', GeneralFeatureCreateView.as_view(), name="feature-create"),
    url(r'^general-features/(?P<feature_id>[0-9]+)/$', GeneralFeatureDetailView.as_view(), name="feature-delete"),
]
