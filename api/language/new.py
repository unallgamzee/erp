# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from rest_framework.permissions import IsAuthenticated

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC

# Language
from api.language.serializers import LanguageCreateSerializer, LanguageSerializer

from rest_framework.permissions import AllowAny


class LanguageCreateView(APIView):

    permission_classes = (AllowAny, )

    def post(self, request, format=None):
        payload = request.data
        response = None
        serializer = LanguageCreateSerializer(data=payload, many=False)

        if serializer.is_valid():
            language = serializer.save()
            detail_serializer = LanguageSerializer(language, read_only=True, many=False)
            response_data = {
                'language': detail_serializer.data
            }
            response = Response(response_data, status=status.HTTP_201_CREATED)
        else:
            generated_status = SG.get_status(_("Parameter error."), SC.parameter_error, serializer.errors)
            response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)

        return response
