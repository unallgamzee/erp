# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Language
from api.language.models import Language
from api.language.serializers import LanguageSerializer

from rest_framework.permissions import AllowAny


class LanguageListView(APIView):

    permission_classes = (AllowAny, )

    def get(self, request, format=None):
        languages = Language.objects.all()
        serializer = LanguageSerializer(languages, read_only=True, many=True)
        payload = {
            'languages': serializer.data
        }

        return Response(payload, status=status.HTTP_200_OK)
