# Django
from django.conf.urls import url

# Language
from api.language.list import LanguageListView
from api.language.new import LanguageCreateView
from api.language.delete import LanguageDeleteView

urlpatterns = [
    url(r'^languages/$', LanguageListView.as_view(), name="language-list"),
    url(r'^languages/new/$', LanguageCreateView.as_view(), name="language-create"),
    url(r'^languages/(?P<language_id>[0-9]+)/$', LanguageDeleteView.as_view(), name="language-delete")
]
