# Django
from django.db import models

# Common
from api.field_names import CommonChoices


class Language(models.Model):
    code = models.CharField(max_length=3, unique=True, null=False, blank=False,
                            choices=CommonChoices.LANGUAGE_CODE_CHOICES, verbose_name='language code')
    name = models.CharField(max_length=64, unique=True,null=False, blank=False)

    @staticmethod
    def get_language(**kwargs):
        language = None
        try:
            language = Language.objects.get(**kwargs)
        except Language.DoesNotExist:
            pass

        return language
