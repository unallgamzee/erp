# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

from rest_framework.permissions import IsAuthenticated

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC

# Language
from api.language.models import Language

from rest_framework.permissions import AllowAny


class LanguageDeleteView(APIView):

    permission_classes = (AllowAny, )

    def delete(self, request, language_id, format=None):
        response = None
        language = Language.get_language(pk=language_id)

        if language is None:
            language = {
                'language-id': [_("A language with this ID doesn't exist")]
            }
            generated_status = SG.get_status(_("Language doesn't exist."), SC.lang_no_exist, None)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)
        else:
            language.delete()
            response = Response(status=status.HTTP_200_OK)

        return response
