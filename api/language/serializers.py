# REST Framework
from rest_framework import serializers

# Language
from api.language.models import Language

# Common
from api.field_names import LanguageFields


class LanguageSerializer(serializers.ModelSerializer):
    """
    @brief    This is used for listing
    """

    class Meta:
        model = Language
        fields = (
            LanguageFields.lang_id,
            LanguageFields.code,
            LanguageFields.name
        )


class LanguageCreateSerializer(serializers.ModelSerializer):
    """
    @brief    This is used for creating a language
    """

    class Meta:
        model = Language
        fields = (
            LanguageFields.code,
            LanguageFields.name
        )
