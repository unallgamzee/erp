# Django
from django.db import models

from api.invoice.models import Invoice
from api.receipt.models import Receipt
from api.expensive.models import Expensive

from api.user.models import User
from api.general_feature.models import GeneralFeature

from api.contact.models import Contact
from api.company.models import Company
from api.field_names import CommonChoices

from django.utils.timezone import now

from django.db.models.signals import post_save, post_delete
from django.dispatch import receiver


class CashFlow(models.Model):
    cash_type = models.IntegerField(null=False, choices=CommonChoices.FINANCE_TYPE_CHOICES)
    description = models.TextField(max_length=256)
    flow_date = models.DateTimeField(default=now)

    contact = models.ForeignKey(Contact, on_delete=models.CASCADE, null=True)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, null=True)

    price = models.FloatField(blank=True, null=False)
    currency = models.IntegerField(null=False, default=2, choices=CommonChoices.CURRENCY_CHOICES)

    invoice = models.ForeignKey(Invoice, on_delete=models.CASCADE, null=True)
    receipt = models.ForeignKey(Receipt, on_delete=models.CASCADE, null=True)
    expensive = models.ForeignKey(Expensive, on_delete=models.CASCADE, null=True)

    insert_user = models.ForeignKey(User, on_delete=models.CASCADE, related_name="insert_cash_flow", blank=False)
    insert_date = models.DateTimeField(default=now, editable=False)

    @staticmethod
    def get_cash_flow(**kwargs):
        """
        @brief    If the cash flow with the given arguments exists, returns the cash flow. Otherwise returns None
        """

        cash_flow = None
        try:
            cash_flow = CashFlow.objects.get(**kwargs)
        except CashFlow.DoesNotExist:
            pass

        return cash_flow

    @receiver(post_save, sender=Receipt)
    def receipt_post_save_handler(sender, instance, **kwargs):
        try:
            if instance.price is not null:
                payload = {}

                payload["cash_type"] = instance.receipt_type
                payload["flow_date"] = instance.receipt_date
                payload["company"] = instance.company
                payload["contact"] = instance.contact
                payload["receipt"] = instance
                payload["price"] = instance.price
                payload["currency"] = instance.currency
                payload["insert_user"] = instance.insert_user
                payload["insert_date"] = instance.insert_date
                payload["description"] = instance.description

                if instance.status == 1:
                    if instance.receipt_type == 2:
                        payload["price"] = instance.price  * -1
                else :
                    if instance.receipt_type == 1:
                        payload["price"] = instance.price  * -1

                    payload["description"] = "IPTAL" + instance.description

                CashFlow.objects.create(**payload)
        except Exception as e:
            print("Receipt error :", e)


    @receiver(post_save, sender=Expensive)
    def expensive_post_save_handler(sender, instance, **kwargs):
        try:
            payload = {}

            general = GeneralFeature.objects.all()[0]
            contact = Contact.objects.get(contact_user__id=instance.insert_user.id)

            payload["cash_type"] = 2
            payload["flow_date"] = instance.expense_date
            payload["company"] = general.company
            payload["contact"] = contact
            payload["expensive"] = instance
            payload["price"] = instance.price * -1
            payload["currency"] = instance.currency
            payload["insert_user"] = instance.insert_user
            payload["insert_date"] = instance.insert_date
            payload["description"] = instance.description

            CashFlow.objects.create(**payload)
        except Exception as e:
            print("Expensive post error :", e)

    @receiver(post_delete, sender=Expensive)
    def expensive_post_delete_handler(sender, instance, **kwargs):
        try:
            CashFlow.objects.delete(expensive=instance)
        except Exception as e:
            print("Expensive delete error :", e)


    @receiver(post_save, sender=Invoice)
    def invoice_post_save_handler(sender, instance, **kwargs):
        try:
            payload = {}

            payload["cash_type"] = instance.invoice_type
            payload["flow_date"] = instance.invoice_date
            payload["company"] = instance.company
            payload["contact"] = instance.contact
            payload["invoice"] = instance
            payload["price"] = instance.price
            payload["currency"] = instance.currency
            payload["insert_user"] = instance.insert_user
            payload["insert_date"] = instance.insert_date
            payload["description"] = instance.description

            if instance.status == 1:
                if instance.invoice_type == 2:
                    payload["price"] = instance.price  * -1
            else :
                if instance.invoice_type == 1:
                    payload["price"] = instance.price  * -1

                payload["description"] = "IPTAL" + instance.description

            CashFlow.objects.create(**payload)
        except Exception as e:
            print("Invoice error :", e)


