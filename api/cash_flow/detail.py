# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Cash Flow
from api.cash_flow.models import CashFlow

# Serializers
from api.cash_flow.serializers import CashFlowSummarySerializer

from rest_framework.permissions import IsAuthenticated

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC


class CashFlowDetailView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, cash_flow_id, format=None):
        cash_flow = CashFlow.get_cash_flow(id=cash_flow_id)
        if cash_flow is not None:
            serializer = CashFlowSummarySerializer(cash_flow, many=False, read_only=True)
            response_data = {
                "cash-flow": serializer.data
            }

            response = Response(response_data, status=status.HTTP_200_OK)

        else:
            errors = {
                "cash-flow-id": [
                    _("Cash Flow ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A cash flow with the given ID doesn't exist."),
                                             SC.cash_flow_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def delete(self, request, cash_flow_id, format=None):
        cash_flow = CashFlow.get_cash_flow(id=cash_flow_id)
        if cash_flow is not None:
            serializer = CashFlowSummarySerializer(cash_flow, many=False, read_only=True)
            response_data = {
                "cash-flow": serializer.data
            }

            cash_flow.delete()
            response = Response(response_data, status=status.HTTP_200_OK)
        else:
            errors = {
                "cash-flow-id": [
                    _("Cash Flow ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A cash flow with the given ID doesn't exist."),
                                             SC.cash_flow_not_exist,
                                             errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response
