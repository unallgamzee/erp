# Django
from django.core.paginator import Paginator

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# CashFlow
from api.cash_flow.serializers import CashFlowSummarySerializer
from api.cash_flow.models import CashFlow

# General
from api.field_names import CashFlowFields

from rest_framework.permissions import IsAuthenticated


class CashFlowListView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):

        cashFlows = CashFlow.objects.all()

        serializer = CashFlowSummarySerializer(cashFlows, many=True, read_only=True)
        response_data = {
            "cash-flows": serializer.data
        }

        return Response(response_data, status=status.HTTP_200_OK)