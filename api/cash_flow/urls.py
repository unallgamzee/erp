# Django
from django.conf.urls import url

# Cash Flow
from api.cash_flow.new import CashFlowCreateView
from api.cash_flow.detail import CashFlowDetailView
from api.cash_flow.list import CashFlowListView

urlpatterns = [
    url(r'^cash-flows/new/$', CashFlowCreateView.as_view(), name="cash-flow-new"),
    url(r'^cash-flows/$', CashFlowListView.as_view(), name="cash-flow-list"),
    url(r'^cash-flows/(?P<cash_flow_id>[0-9]+)/$', CashFlowDetailView.as_view(), name="cash-flow-detail")
]
