from rest_framework import serializers

# Cash Flow
from api.cash_flow.models import CashFlow

# General
from api.field_names import CashFlowFields

# Serializers
from api.company.serializers import CompanySummarySerializer
from api.contact.serializers import ContactSummarySerializer
from api.invoice.serializers import InvoiceSummarySerializer
from api.receipt.serializers import ReceiptSummarySerializer
from api.expensive.serializers import ExpensiveSummarySerializer
from api.user.serializers import UserSummarySerializer


class CashFlowCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CashFlow
        fields = (
            CashFlowFields.cash_type,
            CashFlowFields.description,
            CashFlowFields.flow_date,

            CashFlowFields.company,
            CashFlowFields.contact,

            CashFlowFields.price,
            CashFlowFields.currency,

            CashFlowFields.invoice,
            CashFlowFields.receipt,
            CashFlowFields.expensive,

            CashFlowFields.insert_user,
            CashFlowFields.insert_date
        )


class CashFlowSummarySerializer(serializers.ModelSerializer):

    company = CompanySummarySerializer(many=False, read_only=True)
    contact = ContactSummarySerializer(many=False, read_only=True)
    invoice = InvoiceSummarySerializer(many=False, read_only=True)
    receipt = ReceiptSummarySerializer(many=False, read_only=True)
    expensive = ExpensiveSummarySerializer(many=False, read_only=True)
    insert_user = UserSummarySerializer(many=False, read_only=True)

    class Meta:
        model = CashFlow
        fields = (
            CashFlowFields.cash_flow_id,
            CashFlowFields.cash_type,
            CashFlowFields.description,
            CashFlowFields.flow_date,

            CashFlowFields.company,
            CashFlowFields.contact,

            CashFlowFields.price,
            CashFlowFields.currency,

            CashFlowFields.invoice,
            CashFlowFields.receipt,
            CashFlowFields.expensive,

            CashFlowFields.insert_user,
            CashFlowFields.insert_date
        )
