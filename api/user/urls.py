# Django
from django.conf.urls import url

# Session
from api.user.register import UserRegisterView
from api.user.list import UserListView
from api.user.detail import UserDetailView
from api.user.login import Login

urlpatterns = [
    url(r'^users/register/$', UserRegisterView.as_view(), name="user-register"),
    url(r'^users/$', UserListView.as_view(), name="user-list"),
    url(r'^users/(?P<user_id>[0-9]+)/$', UserDetailView.as_view(), name="user-detail"),
    url(r'^login/$', Login.as_view(), name="user-login")
]
