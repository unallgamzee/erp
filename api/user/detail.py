# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# User
from api.user.models import User

# Serializers
from api.user.serializers import UserSummarySerializer, UserUpdateSerializer

# Common
from api.status_generator import StatusGenerator as SG, StatusCodes as SC
from api.field_names import UserFields

from rest_framework.permissions import IsAuthenticated


class UserDetailView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, user_id, format=None):
        try:
            user = User.objects.get(id=user_id, is_active=True)
            serializer = UserSummarySerializer(user, many=False, read_only=True)
            response_data = {
                "user": serializer.data
            }

            response = Response(response_data, status=status.HTTP_200_OK)
        except:
            errors = {
                "user-id": [
                    _("User ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A user with the given ID doesn't exist."), SC.user_not_exist, errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def delete(self, request, user_id, format=None):
        try:
            user = User.objects.get(id=user_id, is_active=True)
            serializer = UserSummarySerializer(user, many=False, read_only=True)
            response_data = {
                "user": serializer.data
            }

            user.is_active = False
            user.save()
            response = Response(response_data, status=status.HTTP_200_OK)
        except:
            errors = {
                "user-id": [
                    _("User ID does not exist")
                ]
            }

            generated_status = SG.get_status(_("A user with the given ID doesn't exist."), SC.user_not_exist, errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def put(self, request, user_id, format=None):
        saved_user = None
        payload = request.data

        try:
            user = User.objects.get(id=user_id, is_active=True)
            user_data = self.extract_user_data(payload, user)
            if len(user_data) == 0:
                response = Response(status=status.HTTP_304_NOT_MODIFIED)
            else:
                errors = {}
                if user_data.get(UserFields.email):
                    user_data[UserFields.username] = user_data[UserFields.email]

                user_serializer = UserUpdateSerializer(user, data=user_data, partial=True, many=False)
                if user_serializer.is_valid():
                    saved_user = user_serializer.save()
                    if user_data.get(UserFields.password):
                        saved_user.set_password(payload.get(UserFields.password))
                        saved_user.save()
                else:
                    errors.update(user_serializer.errors)

                if len(errors) > 0:
                    generated_status = SG.get_status(_("Error occurred while updating the user."), SC.parameter_error,
                                                     errors)
                    response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)
                else:
                    detail_serializer = UserSummarySerializer(saved_user, read_only=True, many=False)
                    response_data = {
                        "user": detail_serializer.data
                    }
                    response = Response(response_data, status=status.HTTP_200_OK)
        except:
            errors = {
                "user-id": [
                    _("User ID doesn't exist.")
                ]
            }
            generated_status = SG.get_status(_("A user with the given ID doesn't exist."), SC.user_not_exist, errors)
            response = Response(generated_status, status=status.HTTP_404_NOT_FOUND)

        return response

    def extract_user_data(self, payload, user):
        user_data = {}

        if UserFields.email in payload:
            temp_value = payload.pop(UserFields.email)
            if user.email != temp_value:
                user_data[UserFields.email] = temp_value

        if UserFields.first_name in payload:
            temp_value = payload.pop(UserFields.first_name)
            if user.first_name != temp_value:
                user_data[UserFields.first_name] = temp_value

        if UserFields.last_name in payload:
            temp_value = payload.pop(UserFields.last_name)
            if user.last_name != temp_value:
                user_data[UserFields.last_name] = temp_value

        if UserFields.email in payload:
            temp_value = payload.pop(UserFields.email)
            if user.email != temp_value:
                user_data[UserFields.email] = temp_value

        if UserFields.is_superuser in payload:
            temp_value = payload.pop(UserFields.is_superuser)
            if user.is_superuser != temp_value:
                user_data[UserFields.is_superuser] = temp_value

        if UserFields.password in payload:
            temp_value = payload.pop(UserFields.password)
            if user.password != temp_value:
                user_data[UserFields.password] = temp_value

        return user_data
