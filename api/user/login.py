# Django
from django.utils.translation import ugettext_lazy as _
from django.contrib.auth import authenticate

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# Common
from api.utils import ApiUtils
from api.field_names import UserFields
from api.status_generator import StatusGenerator as SG, StatusCodes as SC

# erp
from erp.settings import DEBUG as IS_IN_DEVELOPMENT


class Login(APIView):
    def post(self, request, format=None):
        payload = request.data
        response = None
        errors = {}

        email = payload.get(UserFields.email)
        password = payload.get(UserFields.password)
        if email is None:
            errors[UserFields.email] = "This field is required."
        if password is None:
            errors[UserFields.password] = "This field is required."

        if len(errors) > 0:
            generated_response = SG.get_status("Parameter error.", SC.parameter_error, errors)
            response = Response(generated_response, status=status.HTTP_400_BAD_REQUEST)

        else:
            # Check If the user exists
            user = authenticate(username=str(email), password=password)
            if user is not None:
                token = ApiUtils.create_token(user)
                if IS_IN_DEVELOPMENT:
                    response_data = {
                        "token": token
                    }
                    response = Response(response_data, status=status.HTTP_200_OK)
            else:
                errors = {"email": ["User does not exist!"]}
                generated_response = SG.get_status(email, SC.user_not_exist, errors)
                response = Response(generated_response, status=status.HTTP_404_NOT_FOUND)

        return response