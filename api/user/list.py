# Django
from django.core.paginator import Paginator

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# User
from api.user.serializers import UserSummarySerializer
from api.user.models import User

# General
from api.field_names import UserFields

from rest_framework.permissions import IsAuthenticated


class UserListView(APIView):

    permission_classes = (IsAuthenticated, )

    def get(self, request, format=None):

        query_params = request.query_params

        users = User.objects.filter(is_active=True)

        serializer = UserSummarySerializer(users, many=True, read_only=True)
        response_data = {
            "users": serializer.data
        }

        return Response(response_data, status=status.HTTP_200_OK)