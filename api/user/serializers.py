from rest_framework import serializers

# User
from api.user.models import User

# General
from api.field_names import UserFields


class UserCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            UserFields.email,
            UserFields.password,
            UserFields.username,
            UserFields.first_name,
            UserFields.last_name,
            UserFields.is_superuser
        )


class UserSummarySerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            UserFields.user_id,
            UserFields.username,
            UserFields.email,
            UserFields.first_name,
            UserFields.last_name,
            UserFields.date_joined,
            UserFields.is_superuser
        )


class UserUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = (
            UserFields.username,
            UserFields.password,
            UserFields.email,
            UserFields.first_name,
            UserFields.last_name,
            UserFields.is_superuser
        )
