# Django
from django.utils.translation import ugettext_lazy as _

# REST Framework
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework import status

# User
from api.user.serializers import UserCreateSerializer, UserSummarySerializer
from api.field_names import UserFields

# Common
from api.utils import ApiUtils
from api.status_generator import StatusGenerator as SG, StatusCodes as SC

from rest_framework.permissions import AllowAny


class UserRegisterView(APIView):

    permission_classes = (AllowAny, )

    def post(self, request, format=None):
        payload = request.data
        payload[UserFields.username] = payload.get(UserFields.email)

        serializer = UserCreateSerializer(data=payload, many=False)
        if serializer.is_valid():
            user = serializer.save()
            user.set_password(payload.get(UserFields.password))
            user.save()

            token = ApiUtils.create_token(user)

            detail_serializer = UserSummarySerializer(user, many=False, read_only=True)

            response_data = {
                "token": token,
                "user": detail_serializer.data
            }

            response = Response(response_data, status=status.HTTP_201_CREATED)

        else:
            generated_status = SG.get_status(_('Parameter error occurred.'), SC.parameter_error, serializer.errors)
            response = Response(generated_status, status=status.HTTP_400_BAD_REQUEST)

        return response
